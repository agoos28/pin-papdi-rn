import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, Image} from 'react-native';
import {withNavigation} from 'react-navigation';
import styles from './Styles/BackButtonStyles';
import {Colors} from 'ignite-andross/boilerplate/App/Themes';
import Icon from 'react-native-vector-icons/FontAwesome';

class BackButton extends Component {
  static propTypes = {
    onPress: PropTypes.func,
  };
  goBack() {
    if (!this.props.onPress) {
      return this.props.navigation.goBack();
    }
    return this.props.onPress;
  }
  render() {
    return (
      <TouchableOpacity onPress={() => this.goBack()} style={styles.container}>
        <Icon name={'arrow-left'} color={Colors.text} size={14} />
      </TouchableOpacity>
    );
  }
}

export default withNavigation(BackButton);
