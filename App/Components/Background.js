import React, {Component, memo} from 'react';
import {ImageBackground, StyleSheet, KeyboardAvoidingView} from 'react-native';
import PropTypes from 'prop-types';
import Styles from './Styles/BackgroundStyles';
import Images from '../Themes/Images';

export default class Background extends Component {
  static propTypes = {
    children: PropTypes.element,
    source: PropTypes.instanceOf,
    style: PropTypes.object,
  };
  static defaultProps = {
    source: Images.background,
  };
  render() {
    const {children, source, style} = this.props;
    return (
      <ImageBackground
        source={source}
        resizeMode={'cover'}
        style={[Styles.container, style]}>
        <KeyboardAvoidingView style={Styles.container} behavior="padding">
          {children}
        </KeyboardAvoidingView>
      </ImageBackground>
    );
  }
}
