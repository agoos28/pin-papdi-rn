import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button as PaperButton} from 'react-native-paper';
import theme from '../Themes/PaperTheme';
import Styles from './Styles/ButtonStyles';

export default class Button extends Component {
  static propTypes = {
    style: PropTypes.object,
    mode: PropTypes.string,
  };

  render() {
    const {children, mode, style} = this.props;
    return (
      <PaperButton
        style={[
          Styles.button,
          mode === 'outlined' && {backgroundColor: theme.colors.surface},
          style,
        ]}
        labelStyle={Styles.text}
        mode={mode}
        {...this.props}>
        {children}
      </PaperButton>
    );
  }
}
