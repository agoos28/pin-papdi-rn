import React, {Component} from 'react';
// import PropTypes from 'prop-types';
import {View, Text, Image} from 'react-native';
import styles from './Styles/EmptyContentStyle';
import Images from '../Themes/Images';

export default class EmptyContent extends Component {
  static defaultProps = {
    textDescription: 'Masih kosong',
  };

  render() {
    return (
      <View style={[styles.wrapper, this.props.style]}>
        <Image source={Images.logo} style={{width: 80, height: 50}} />
        <Text style={{textAlign: 'center'}}>{this.props.textDescription}</Text>
      </View>
    );
  }
}
