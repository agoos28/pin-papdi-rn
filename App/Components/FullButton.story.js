import React from 'react';
import {storiesOf} from '@storybook/react-native';

import Button from './FullButton';

storiesOf('FullButton')
  .add('Default', () => <Button text="A simple button" />)
  .add('Custom Style', () => (
    <Button text="Style Me Up!" styles={{backgroundColor: 'blue'}} />
  ));
