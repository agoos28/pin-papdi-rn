import React, {Component} from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  ProgressBarAndroid,
  Platform,
  StatusBar,
} from 'react-native';
import {withNavigation} from 'react-navigation';
import styles from './Styles/MainHeaderStyle';
import {Colors, Metrics} from '../Themes/index';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Appbar} from 'react-native-paper';
import theme from '../Themes/PaperTheme';
import AuthActions from '../Redux/AuthRedux';
import {connect} from 'react-redux';

class MainHeader extends Component {
  static defaultProps = {
    title: null,
    subtitleComponent: null,
    showBackButton: false,
    placement: 'center',
    backgroundColor: Colors.background,
    leftComponent: null,
    centerComponent: null,
    left: [],
    right: [],
    rightComponent: null,
    badgeCounts: {},
    containerStyle: null,
    loading: false,
  };

  static renderBadge(cart) {
    const count = cart ? cart.length : 0;
    if (count > 0) {
      return (
        <View style={styles.badge}>
          <Text style={styles.badgeCount}>{count}</Text>
        </View>
      );
    } else {
      return null;
    }
  }

  renderLeftComponent() {
    if (!this.props.left.length) {
      return null;
    }
    const arr = [];
    this.props.left.map((th) => {
      if (typeof th === 'object') {
        if (th.type === 'customBack') {
          arr.push(<Appbar.BackAction onPress={() => th.action()} />);
        }
      }
      if (th === 'back') {
        arr.push(
          <Appbar.BackAction
            onPress={() => this.props.navigation.goBack(null)}
          />,
        );
      }
    });
    if (this.props.leftComponent) {
      arr.push(this.props.leftComponent);
    }
    return arr;
  }

  renderRightComponent() {
    const self = this;
    const arr = [];
    if (!this.props.right) {
      return null;
    }
    this.props.right.map((th, index) => {
      if (typeof th === 'object') {
        if (th.type === 'textNavigation') {
          arr.push(
            <TouchableOpacity
              style={[styles.item]}
              key={index}
              activeOpacity={0.8}
              onPress={() =>
                self.props.navigation.navigate({
                  routeName: th.routeName,
                  params: null,
                })
              }>
              <Text
                style={{fontSize: 12, letterSpacing: -0.27, color: '#ffffff'}}>
                {th.text}
              </Text>
            </TouchableOpacity>,
          );
        }
        if (th.type === 'action') {
          arr.push(
            <TouchableOpacity
              style={styles.item}
              key={index}
              activeOpacity={0.8}
              onPress={th.action}>
              <Icon
                name={th.icon}
                size={Metrics.designRatio * 20}
                color={Colors.background}
              />
            </TouchableOpacity>,
          );
        }
      }
      if (th === 'chat') {
        arr.push(
          <TouchableOpacity
            style={styles.item}
            key={index}
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate(
                global.isGuest ? 'SignInScreen' : 'Messages',
              )
            }>
            <Icon
              name="speak"
              size={Metrics.designRatio * 20}
              color={Colors.background}
            />
          </TouchableOpacity>,
        );
      }
      if (th === 'cart') {
        arr.push(
          <TouchableOpacity
            style={[styles.item, {marginRight: 10}]}
            key={index}
            activeOpacity={0.8}
            onPress={() => {
              if (this.props.cart) {
                this.props.navigation.navigate('Cart');
              }
            }}>
            {MainHeader.renderBadge(this.props.cart)}
            <Icon
              name="shopping-cart"
              size={Metrics.designRatio * 20}
              color={Colors.background}
            />
          </TouchableOpacity>,
        );
      }
      if (th === 'notification') {
        arr.push(
          <TouchableOpacity
            style={styles.item}
            key={index}
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate(
                global.isGuest ? 'SignInScreen' : 'NotificationScreen',
              )
            }>
            {MainHeader.renderBadge(this.props.badgeCounts.notification)}
            <Icon
              name="bells"
              size={Metrics.designRatio * 20}
              color={Colors.background}
            />
          </TouchableOpacity>,
        );
      }
      if (th === 'message') {
        arr.push(
          <TouchableOpacity
            style={styles.item}
            key={index}
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate(
                global.isGuest ? 'SignInScreen' : 'NotificationScreen',
              )
            }>
            {MainHeader.renderBadge(this.props.badgeCounts.message)}
            <Icon
              name="mailbox"
              size={Metrics.designRatio * 20}
              color={Colors.background}
            />
          </TouchableOpacity>,
        );
      }
      if (th === 'plus') {
        arr.push(
          <TouchableOpacity style={styles.item} key={index} activeOpacity={0.8}>
            <Icon
              name="close"
              size={Metrics.designRatio * 20}
              color={Colors.background}
            />
          </TouchableOpacity>,
        );
      }
      if (th === 'profile') {
        arr.push(
          <TouchableOpacity
            style={styles.item}
            key={index}
            activeOpacity={0.8}
            onPress={() => {
              if (this.props.auth.guest) {
                this.props.navigation.navigate('Login');
              } else {
                this.props.navigation.navigate('Account');
              }
            }}>
            <Icon
              name="user"
              size={Metrics.designRatio * 20}
              color={Colors.background}
            />
          </TouchableOpacity>,
        );
      }
      if (th === 'setting') {
        arr.push(
          <TouchableOpacity
            style={styles.item}
            key={index}
            activeOpacity={0.8}
            onPress={() => this.props.navigation.navigate('SettingScreen')}>
            <Icon
              name="support"
              size={Metrics.designRatio * 20}
              color={Colors.background}
            />
          </TouchableOpacity>,
        );
      }
      if (th === 'close') {
        arr.push(
          <TouchableOpacity
            style={styles.item}
            key={index}
            activeOpacity={0.8}
            onPress={() => this.props.navigation.goBack()}>
            <Icon
              name="cancel-1"
              size={Metrics.designRatio * 20}
              color={Colors.background}
            />
          </TouchableOpacity>,
        );
      }
    });
    if (this.props.rightComponent) {
      arr.push(this.props.rightComponent);
    }
    return arr;
  }

  render() {
    return (
      <React.Fragment>
        <Appbar.Header dark={true} style={{backgroundColor: Colors.primary}}>
          {this.renderLeftComponent()}
          <Appbar.Content
            title={this.props.title}
            subtitle={this.props.subtitle}
          />
          {this.renderRightComponent()}
        </Appbar.Header>
        <StatusBar barStyle={'light-content'} animated={true} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    cart: state.event.cart,
  };
};

export default connect(mapStateToProps)(withNavigation(MainHeader));
