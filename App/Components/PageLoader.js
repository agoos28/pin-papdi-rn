import React, {Component} from 'react';
// import PropTypes from 'prop-types';
import {ProgressBarAndroid} from 'react-native';
import styles from './Styles/PageLoaderStyle';

export default class PageLoader extends Component {
  static defaultProps = {
    loading: false,
  };

  render() {
    if (this.props.loading) {
      return (
        <ProgressBarAndroid
          styleAttr={'Horizontal'}
          animating
          style={styles.pageProgressBar}
          color={'#ccc'}
        />
      );
    } else {
      return null;
    }
  }
}
