import React, {Component} from 'react';
import PropTypes from 'prop-types';
import theme from '../Themes/PaperTheme';
import {Text} from 'react-native-paper';

export default class Status extends Component {
  static propTypes = {
    status: PropTypes.string,
  };

  render() {
    const {status} = this.props;
    const style = {
      borderRadius: 10,
      paddingVertical: 3,
      paddingHorizontal: 5,
      fontSize: 12,
      width: 65,
      textAlign: 'center',
    };
    switch (status) {
      case '10':
      case '13':
        return (
          <Text
            style={{
              ...style,
              backgroundColor: theme.colors.warning,
              color: theme.colors.background,
            }}>
            Pending
          </Text>
        );
      case '12':
        return (
          <Text
            style={{
              ...style,
              backgroundColor: theme.colors.success,
              color: '#fff',
            }}>
            Lunas
          </Text>
        );
      case '14':
        return (
          <Text
            style={{
              ...style,
              backgroundColor: theme.colors.disabled,
              color: '#fff',
            }}>
            Expired
          </Text>
        );
      default:
        return (
          <Text
            style={{
              ...style,
              backgroundColor: theme.colors.disabled,
            }}>
            {status}
          </Text>
        );
    }
  }
}
