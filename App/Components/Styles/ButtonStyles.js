import {StyleSheet} from 'react-native';
import theme from '../../Themes/PaperTheme';
import {Colors} from '../../Themes';

export default StyleSheet.create({
  button: {
    width: '100%',
    marginVertical: 10,
    backgroundColor: theme.colors.primary,
  },
  text: {
    color: Colors.silver,
    fontWeight: '300',
    fontSize: 15,
    lineHeight: 26,
  },
});
