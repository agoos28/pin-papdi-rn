import {StyleSheet} from 'react-native';
import {Metrics} from '../../Themes/index';
import Colors from '../../Themes/Colors';

export default StyleSheet.create({
  bar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Colors.background,
    paddingVertical: 15,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    color: Colors.charcoal,
  },
  item: {
    paddingLeft: Metrics.designRatio * 12,
    paddingRight: Metrics.designRatio * 12,
  },
  badge: {
    fontFamily: 'Montserrat-Medium',
    position: 'absolute',
    zIndex: 1,
    backgroundColor: '#fdc20b',
    borderRadius: 15,
    height: Metrics.designRatio * 20,
    width: Metrics.designRatio * 20,
    top: -7,
    right: -2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  badgeCount: {
    color: '#ffffff',
    fontSize: 12,
  },
  pageProgressBar: {
    marginTop: -7,
    marginBottom: -5,
    height: 16,
  },
});
