import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  pageProgressBar: {
    marginTop: -7,
    marginBottom: -5,
    height: 16,
    backgroundColor: 'rgba(0,0,0,0)',
    position: 'absolute',
    left: 0,
    right: 0,
    zIndex: 99,
  },
});
