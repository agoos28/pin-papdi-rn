// Simple React Native specific changes

import '../I18n/I18n';

export default {
  // font scaling override - RN default is on
  // apiEndpoint: __DEV__
  //   ? 'http://192.168.1.141/php5/pin_papdi/'
  //   : 'https://devpin.papdi.or.id/',
  apiEndpoint: 'https://devpin.papdi.or.id/',
  allowTextFontScaling: true,
};
