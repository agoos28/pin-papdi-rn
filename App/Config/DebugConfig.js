export default {
  showDevScreens: false,
  useFixtures: false,
  ezLogin: false,
  yellowBox: false,
  reduxLogging: __DEV__,
  includeExamples: false,
  useReactotron: false,
  useReduxDevTools: __DEV__,
};
