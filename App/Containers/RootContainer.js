import React, {Component} from 'react';
import {View, StatusBar, Platform} from 'react-native';
import ReduxNavigation from '../Navigation/ReduxNavigation';
import {connect} from 'react-redux';

// Styles
import styles from './Styles/RootContainerStyles';
import theme from '../Themes/PaperTheme';

class RootContainer extends Component {
  componentDidMount() {
    // if redux persist is not active fire startup action
  }

  render() {
    return (
      <View style={styles.applicationView}>
        <StatusBar
          animated={true}
          barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'}
          backgroundColor={theme.colors.primary}
        />
        <ReduxNavigation />
      </View>
    );
  }
}

// wraps dispatch to create nicer functions to call within our component
const mapDispatchToProps = (dispatch) => ({});

export default connect(null, mapDispatchToProps)(RootContainer);
