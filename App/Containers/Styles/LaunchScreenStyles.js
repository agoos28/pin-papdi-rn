import {StyleSheet} from 'react-native';
import {Metrics, BaseStyle} from '../../Themes/';

export default StyleSheet.create({
  ...BaseStyle.screen,
  container: {
    paddingBottom: Metrics.baseMargin,
  },
  logo: {
    marginTop: Metrics.doubleSection,
    height: Metrics.images.logo,
    width: Metrics.images.logo,
    resizeMode: 'contain',
  },
  centered: {
    alignItems: 'center',
  },
});
