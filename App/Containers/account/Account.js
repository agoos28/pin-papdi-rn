import React, {Component} from 'react';
import {ScrollView, Text, View} from 'react-native';
import {connect} from 'react-redux';
import styles from './Account.style';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AuthActions from '../../Redux/AuthRedux';
import Button from '../../Components/Button';
import {NavigationActions} from 'react-navigation';
import {
  Card,
  Divider,
  List,
  Avatar,
  Title,
  Subheading,
  Badge,
} from 'react-native-paper';
import MainHeader from '../../Components/MainHeader';
import images from '../../Themes/Images';
import AppConfig from '../../Config/AppConfig';

class Account extends Component {
  constructor(props) {
    super(props);
    const {user} = props.auth;
    this.state = {};
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.auth.user !== this.props.auth.user &&
      this.props.auth.user === null
    ) {
      this.props.navigation.reset(
        [NavigationActions.navigate({routeName: 'Home'})],
        0,
      );
    }
  }

  render() {
    const {auth} = this.props;
    if (!auth.user) {
      return null;
    }
    return (
      <View style={{flex: 1}}>
        <MainHeader
          left={['back']}
          right={['cart']}
          title={'Akun'}
          cart={this.props.cart}
        />
        <ScrollView>
          <View style={styles.accountBanner}>
            <Avatar.Image
              size={128}
              source={{
                uri:
                  AppConfig.apiEndpoint +
                  'images/propict/' +
                  auth.user.id +
                  '.png',
              }}
              defaultSource={images.defaultUser}
            />
            <Title>{auth.user.name}</Title>
            <Subheading>{auth.user.email}</Subheading>
          </View>
          <Card style={{marginBottom: 5}}>
            <List.Section style={{paddingVertical: 0}}>
              <List.Item
                title={'Profil'}
                right={() => (
                  <View style={styles.listItemRight}>
                    <List.Icon icon="chevron-right" />
                  </View>
                )}
                onPress={() => this.props.navigation.navigate('Profile')}
              />
              <Divider />
              <List.Item
                title={'Booking Saya'}
                right={() => (
                  <View style={styles.listItemRight}>
                    <List.Icon icon="chevron-right" />
                  </View>
                )}
                onPress={() => this.props.navigation.navigate('MyEvent')}
              />
              <Divider />
              <List.Item
                title={'Transaksi Saya'}
                right={() => (
                  <View style={styles.listItemRight}>
                    <List.Icon icon="chevron-right" />
                  </View>
                )}
                onPress={() => this.props.navigation.navigate('Transaction')}
              />
              <Divider />
              <List.Item
                title={'Logout'}
                right={() => <List.Icon icon="chevron-right" />}
                onPress={() => {
                  this.props.logout();
                }}
              />
            </List.Section>
          </Card>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    cart: state.event.cart,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    logout: (params) => dispatch(AuthActions.authRequest('logout', params)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Account);
