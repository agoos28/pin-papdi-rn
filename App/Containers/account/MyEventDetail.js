import React, {Component} from 'react';
import {Image, Linking, ScrollView, Text, View} from 'react-native';
import {connect} from 'react-redux';
import styles from './Account.style';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AuthActions from '../../Redux/AuthRedux';
import Button from '../../Components/Button';
import {NavigationActions} from 'react-navigation';
import API from '../../Services/Api';
import {getValue} from '../../Helper/Validator';
import MainHeader from '../../Components/MainHeader';
import {Col, Grid} from 'react-native-easy-grid';
import {
  ActivityIndicator,
  Card,
  Divider,
  Paragraph,
  Subheading,
} from 'react-native-paper';
import Status from '../../Components/Status';
import {formatDateTime, formatMoney, formatTime} from '../../Helper/Formater';
const api = API.create();

class MyEventDetail extends Component {
  constructor(props) {
    super(props);
    const navigationParams = props.navigation.getParam('params', null);
    this.state = {
      id: navigationParams,
      detail: null,
      isLoading: false,
    };
  }

  async getDetail() {
    const {id, isLoading} = this.state;
    if (isLoading) {
      return;
    }
    this.setState({
      isLoading: true,
    });
    const request = await api.transaction('getBookingDetail', {id: id});
    const result = getValue(request, 'data.data', {default: null});
    this.setState({
      detail: result ? result : null,
      isLoading: false,
    });
  }

  componentDidMount() {
    this.getDetail();
  }

  renderInfoItem(item, index) {
    return (
      <React.Fragment key={index}>
        <Grid>
          <Col style={{paddingVertical: 8}}>
            <Subheading>{item.label}</Subheading>
          </Col>
          <Col style={{paddingVertical: 8}}>
            <Subheading style={{textAlign: 'right'}}>{item.data}</Subheading>
          </Col>
        </Grid>
        <Divider />
      </React.Fragment>
    );
  }

  renderBooking() {
    const {detail} = this.state;
    const {booking, qr} = detail;
    if (!booking) {
      return null;
    }

    let bookingData = [];

    bookingData.push({
      label: 'Mulai',
      data: formatDateTime(booking.event.start_date),
    });

    bookingData.push({
      label: 'Berakhir',
      data: formatDateTime(booking.event.end_date),
    });

    return (
      <React.Fragment>
        <View style={{padding: 20, alignItems: 'center'}}>
          <Image style={{width: 250, height: 250}} source={{uri: qr}} />
        </View>
        <Card style={{marginBottom: 8}}>
          <Card.Title title={booking.event.title} />
          <Divider />
          <Card.Content>
            {bookingData.map((item, index) => this.renderInfoItem(item, index))}
            <Grid>
              <Col style={{paddingVertical: 8}}>
                <Subheading>Status</Subheading>
              </Col>
              <Col style={{paddingTop: 14, alignItems: 'flex-end'}}>
                <Status status={booking.status} />
              </Col>
            </Grid>
          </Card.Content>
        </Card>
      </React.Fragment>
    );
  }

  renderWorkshops() {
    const {detail} = this.state;
    const {workshops} = detail;
    if (!workshops) {
      return null;
    }
    return (
      <Card style={{marginBottom: 8}}>
        <Card.Title title={'Agenda Workshop'} />
        <Divider />
        <Card.Content>
          {workshops.map((item, index) => (
            <React.Fragment key={index}>
              <View style={{paddingVertical: 16}}>
                <Paragraph style={{fontWeight: 'bold'}}>
                  {item.code} - {item.name}
                </Paragraph>
                <Paragraph style={{color: '#828282'}}>
                  {formatDateTime(item.timeline?.start_time)} s/d{' '}
                  {formatTime(item.timeline?.end_time)}
                </Paragraph>
                {this.renderLinks(item.links)}
              </View>
              <Divider />
            </React.Fragment>
          ))}
        </Card.Content>
      </Card>
    );
  }

  renderLinks(links) {
    const _links = [];
    const labels = ['Zoom Link', 'Pretest Link', 'PostTest Link'];

    Object.keys(links).map((item, index) => {
      _links.push(
        <Button
          key={index}
          compact={true}
          mode={'contained'}
          style={{flex: 0, marginRight: 8}}
          labelStyle={{
            fontSize: 12,
          }}
          color={index ? '#0275c1' : undefined}
          disabled={!links[item]}
          onPress={async () => {
            const supported = await Linking.canOpenURL(links[item]);
            if (supported) {
              await Linking.openURL(links[item]);
            }
          }}>
          {labels[index]}
        </Button>,
      );
    });
    return <View style={{flexDirection: 'row', marginTop: 8}}>{_links}</View>;
  }

  render() {
    const {detail} = this.state;
    if (!detail) {
      return (
        <View style={{flex: 1}}>
          <MainHeader title={'Detil Booking'} left={['back']} />
          <View style={{paddingVertical: 100}}>
            <ActivityIndicator animating={true} color={'#000'} />
          </View>
        </View>
      );
    }
    return (
      <View style={{flex: 1}}>
        <MainHeader title={'Detil Booking'} left={['back']} />
        <ScrollView>
          {this.renderBooking()}
          {this.renderWorkshops()}
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    logout: (params) => dispatch(AuthActions.authSuccess('logout', params)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyEventDetail);
