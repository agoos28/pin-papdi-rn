import React, {Component} from 'react';
import {ScrollView, Text, View} from 'react-native';
import {connect} from 'react-redux';
import styles from './Account.style';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AuthActions from '../../Redux/AuthRedux';
import Button from '../../Components/Button';
import {NavigationActions} from 'react-navigation';
import {
  Card,
  Divider,
  List,
  Avatar,
  Title,
  Subheading,
  Badge,
} from 'react-native-paper';
import MainHeader from '../../Components/MainHeader';
import images from '../../Themes/Images';
import AppConfig from '../../Config/AppConfig';
import {Col, Grid} from 'react-native-easy-grid';

class Profile extends Component {
  constructor(props) {
    super(props);
    const {user} = props.auth;
    this.state = {};
  }

  renderInfoItem(item, index) {
    return (
      <React.Fragment key={index}>
        <Grid>
          <Col style={{paddingVertical: 8}}>
            <Subheading>{item.label}</Subheading>
          </Col>
          <Col style={{paddingVertical: 8}}>
            <Subheading style={{textAlign: 'right'}}>{item.data}</Subheading>
          </Col>
        </Grid>
        <Divider />
      </React.Fragment>
    );
  }

  renderInfo() {
    const {user} = this.props.auth;
    const profile = [
      {label: 'Nama', data: user.name},
      {label: 'Email', data: user.email},
      {label: 'Telpon', data: user.phone},
      {label: 'Akses', data: user.usertype},
      {label: 'Terdaftar', data: user.registerDate},
      {label: 'Terakhir Login', data: user.lastvisitDate},
      {label: 'Negara', data: user.country},
      {label: 'Kota', data: user.city},
      {label: 'Propinsi', data: user.province},
      {label: 'Kode Pos', data: user.postal},
      {label: 'Alamat', data: user.address},
    ];
    return (
      <React.Fragment>
        <Card>
          <Card.Content>
            {profile.map((item, index) => this.renderInfoItem(item, index))}
          </Card.Content>
        </Card>
      </React.Fragment>
    );
  }

  render() {
    const {auth} = this.props;
    return (
      <View style={{flex: 1}}>
        <MainHeader
          left={['back']}
          right={['cart']}
          title={'Profil'}
          cart={this.props.cart}
        />
        <ScrollView>
          {this.renderInfo()}
          {/*<View style={{padding: 8}}>
            <Button
              mode={'contained'}
              onPress={() => null}
              style={{marginBottom: 8}}>
              Update Profil
            </Button>
            <Button
              mode={'contained'}
              onPress={() => null}
              style={{marginBottom: 8}}>
              Ganti Password
            </Button>
          </View>*/}
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    cart: state.event.cart,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    logout: (params) => dispatch(AuthActions.authSuccess('logout', params)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
