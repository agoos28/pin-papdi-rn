import React, {Component} from 'react';
import {FlatList, RefreshControl, View} from 'react-native';
import {connect} from 'react-redux';
import styles from './Account.style';
import AuthActions from '../../Redux/AuthRedux';
import API from '../../Services/Api';
import {getValue} from '../../Helper/Validator';
import {
  ActivityIndicator,
  Caption,
  Divider,
  List,
  Subheading,
} from 'react-native-paper';
import Status from '../../Components/Status';
import {formatDateTime} from '../../Helper/Formater';
import MainHeader from '../../Components/MainHeader';
const api = API.create();

class Transaction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      transaction: [],
      pagination: {
        start: 0,
        limit: 0,
        total: 0,
      },
      isLoading: false,
      refreshing: false,
    };
  }

  componentDidMount() {
    this.getTransaction(false);
  }

  async getTransaction(refresh) {
    const {transaction, pagination, isLoading} = this.state;
    if (isLoading) {
      return;
    }
    const reqParams = {
      start: refresh ? 0 : pagination.start + pagination.limit,
    };
    if (reqParams.start > pagination.total) {
      return;
    }
    this.setState({
      isLoading: true,
      refreshing: refresh,
    });
    const request = await api.transaction('getTransaction', reqParams);
    const result = getValue(request, 'data.data', {default: null});
    if (result) {
      this.setState({
        transaction: refresh
          ? result.transaction
          : [...transaction, ...result.transaction],
        pagination: result.pagination,
        isLoading: false,
        refreshing: false,
      });
    } else {
      this.setState({
        isLoading: false,
        refreshing: false,
      });
    }
  }

  renderItem(item) {
    return (
      <React.Fragment>
        <List.Item
          left={() => (
            <View style={{marginLeft: 8}}>
              <Subheading style={{marginVertical: 0}}>
                {'Transaksi ID-' + item.id}
              </Subheading>
              <Caption style={{marginVertical: 0}}>
                {formatDateTime(item.created_date)}
              </Caption>
              <Subheading style={{marginVertical: 0}}>
                {'Rp.' + item.value}
              </Subheading>
            </View>
          )}
          right={() => (
            <View style={styles.listItemRight}>
              <Status status={item.status} />
              <List.Icon icon="chevron-right" />
            </View>
          )}
          onPress={() =>
            this.props.navigation.navigate('TransactionDetail', {
              params: item.id,
            })
          }
        />
        <Divider />
      </React.Fragment>
    );
  }

  renderListFooter() {
    const {isLoading} = this.state;
    if (isLoading) {
      return (
        <View style={{paddingVertical: 100}}>
          <ActivityIndicator animating={true} color={'#000'} />
        </View>
      );
    }
    return null;
  }

  render() {
    const {transaction} = this.state;
    return (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <MainHeader title={'Transaksi'} left={['back']} />
        <FlatList
          data={transaction}
          renderItem={({item}) => this.renderItem(item)}
          keyExtractor={(item, index) => index.toString()}
          onEndReached={() => this.getTransaction(false)}
          ListFooterComponent={() => this.renderListFooter()}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this.getTransaction(true)}
            />
          }
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    logout: (params) => dispatch(AuthActions.authSuccess('logout', params)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Transaction);
