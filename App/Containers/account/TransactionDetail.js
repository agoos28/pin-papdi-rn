import React, {Component} from 'react';
import {ScrollView, Text, View} from 'react-native';
import {connect} from 'react-redux';
import styles from './Account.style';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AuthActions from '../../Redux/AuthRedux';
import Button from '../../Components/Button';
import {NavigationActions} from 'react-navigation';
import API from '../../Services/Api';
import {getValue} from '../../Helper/Validator';
import MainHeader from '../../Components/MainHeader';
import {Col, Grid} from 'react-native-easy-grid';
import {
  ActivityIndicator,
  Card,
  Divider,
  Paragraph,
  Subheading,
} from 'react-native-paper';
import Status from '../../Components/Status';
import {formatDateTime, formatMoney} from '../../Helper/Formater';
const api = API.create();

class TransactionDetail extends Component {
  constructor(props) {
    super(props);
    const navigationParams = props.navigation.getParam('params', null);
    this.state = {
      id: navigationParams,
      detail: null,
      isLoading: false,
    };
  }

  async getDetail() {
    const {id, isLoading} = this.state;
    if (isLoading) {
      return;
    }
    this.setState({
      isLoading: true,
    });
    const request = await api.transaction('getTransactionDetail', {id: id});
    const result = getValue(request, 'data.data', {default: null});
    this.setState({
      detail: result ? result : null,
      isLoading: false,
    });
  }

  componentDidMount() {
    this.getDetail();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.nav.index > this.props.nav.index) {
      this.getDetail();
    }
  }

  async snapPay(transactionId) {
    const reqParams = {
      id: transactionId,
      task: 'payment',
      ajax: 1,
    };

    const request = await api.event('cart', reqParams);
    const result = getValue(request, 'data', {default: null});

    if (result) {
      this.props.navigation.navigate('SnapPayment', {
        params: {token: result.token},
      });
    }
  }

  renderInfoItem(item, index) {
    return (
      <React.Fragment key={index}>
        <Grid>
          <Col style={{paddingVertical: 8}}>
            <Subheading>{item.label}</Subheading>
          </Col>
          <Col style={{paddingVertical: 8}}>
            <Subheading style={{textAlign: 'right'}}>{item.data}</Subheading>
          </Col>
        </Grid>
        <Divider />
      </React.Fragment>
    );
  }

  renderTransaction() {
    const {detail} = this.state;
    const {transaction} = detail;
    if (!transaction) {
      return null;
    }
    return (
      <Card style={{marginBottom: 8}}>
        <Card.Title title={'Transaksi ID-' + transaction.id} />
        <Divider />
        <Card.Content>
          <Grid>
            <Col size={1} style={{paddingVertical: 8}}>
              <Subheading>{'Status'}</Subheading>
            </Col>
            <Col size={2} style={{paddingVertical: 8, alignItems: 'flex-end'}}>
              <Status status={transaction.status} />
            </Col>
          </Grid>
          <Divider />
          <Grid>
            <Col style={{paddingVertical: 8}}>
              <Subheading>{'Total'}</Subheading>
            </Col>
            <Col style={{paddingVertical: 8, alignItems: 'flex-end'}}>
              <Subheading style={{textAlign: 'right'}}>
                Rp.{formatMoney(transaction.value)}
              </Subheading>
            </Col>
          </Grid>
        </Card.Content>
      </Card>
    );
  }

  renderPayment() {
    const {detail} = this.state;
    const {payment, transaction} = detail;
    if (!payment) {
      return null;
    }

    let paymentData = [];

    if (payment.method === 'midtrans') {
      paymentData.push({
        label: 'Metode Bayar',
        data: 'Midtrans',
      });

      if (payment.status === '201') {
        paymentData.push({
          label: 'Status',
          data: 'Menunggu Pembayaran',
        });
      }

      if (payment.status === '200') {
        paymentData.push({label: 'Status', data: 'Sukses'});
        paymentData.push({
          label: 'Tgl Bayar',
          data: formatDateTime(payment.time),
        });
      }

      if (payment.va) {
        paymentData.push({
          label: 'Kode Bayar',
          data: payment.va,
        });
      }

      if (payment.bank) {
        paymentData.push({
          label: 'Metode Bayar',
          data: payment.type + ' ' + payment.bank.toUpperCase(),
        });
      }
    }

    if (payment.method === 'commitment') {
      paymentData.push({
        label: 'Metode Bayar',
        data: 'Surat Komitment',
      });
    }

    if (!paymentData.length) {
      return null;
    }

    return (
      <Card style={{marginBottom: 16}}>
        <Card.Title title={'Detil Pembayaran'} />
        <Divider />
        <Card.Content>
          {paymentData.map((item, index) => this.renderInfoItem(item, index))}
          {this.renderPaymentButton()}
        </Card.Content>
      </Card>
    );
  }

  renderPaymentButton() {
    const {detail} = this.state;
    const {payment, transaction} = detail;

    if (payment.status === null && payment.method === 'midtrans') {
      return (
        <View style={{padding: 0}}>
          <Button
            mode={'contained'}
            onPress={() => this.snapPay(transaction.id)}
            style={{marginTop: 8}}>
            Lanjutkan Pembayaran
          </Button>
        </View>
      );
    }

    return null;
  }

  renderPeserta() {
    const el = [];
    const {detail} = this.state;
    if (!detail || !detail.detail) {
      return null;
    }
    detail.detail.map((item, index) => {
      const {name, email, phone, npa, bidang, workshops} = item;
      el.push(
        <Card key={index} style={{marginBottom: 8}}>
          <Card.Title title={'Preserta ' + (index + 1)} />
          <Divider />
          <Card.Content>
            <Grid>
              <Col style={{paddingVertical: 8}}>
                <Subheading style={{textAlign: 'left'}}>{name}</Subheading>
              </Col>
            </Grid>
            <Divider />
            <Grid>
              <Col size={1} style={{paddingVertical: 8}}>
                <Paragraph>Email</Paragraph>
              </Col>
              <Col size={2} style={{paddingVertical: 8}}>
                <Paragraph style={{textAlign: 'right'}}>{email}</Paragraph>
              </Col>
            </Grid>
            <Divider />
            <Grid>
              <Col style={{paddingVertical: 8}}>
                <Paragraph>Telepon</Paragraph>
              </Col>
              <Col style={{paddingVertical: 8}}>
                <Paragraph style={{textAlign: 'right'}}>{phone}</Paragraph>
              </Col>
            </Grid>
            <Divider />
            <Grid>
              <Col style={{paddingVertical: 8}}>
                <Paragraph>Keanggotaan</Paragraph>
              </Col>
              <Col style={{paddingVertical: 8}}>
                <Paragraph style={{textAlign: 'right'}}>
                  {npa ? 'Anggota' : 'Non-anggota'}
                </Paragraph>
              </Col>
            </Grid>
            <Divider />
            <Grid>
              <Col style={{paddingVertical: 8}}>
                <Paragraph>Bidang</Paragraph>
              </Col>
              <Col style={{paddingVertical: 8}}>
                <Paragraph style={{textAlign: 'right'}}>{bidang}</Paragraph>
              </Col>
            </Grid>
            <Divider />
            <Paragraph style={{paddingVertical: 8}}>Pilihan Workshop</Paragraph>
            {workshops &&
              workshops.map((item, index) => (
                <Grid key={index}>
                  <Col style={{width: 20}}>
                    <Paragraph>{index + 1}</Paragraph>
                  </Col>
                  <Col>
                    <Paragraph>{item.name}</Paragraph>
                  </Col>
                </Grid>
              ))}
          </Card.Content>
        </Card>,
      );
    });
    return el;
  }

  render() {
    const {detail, isLoading} = this.state;
    if (isLoading) {
      return (
        <View style={{flex: 1}}>
          <MainHeader title={'Detil Transaksi'} left={['back']} />
          <View style={{paddingVertical: 100}}>
            <ActivityIndicator animating={true} color={'#000'} />
          </View>
        </View>
      );
    }
    if (!detail) {
      return (
        <View style={{flex: 1}}>
          <MainHeader title={'Detil Transaksi'} left={['back']} />
          <View style={{paddingVertical: 100}}>
            <Paragraph>{'Data tidak ditemukan!'}</Paragraph>
          </View>
        </View>
      );
    }
    return (
      <View style={{flex: 1}}>
        <MainHeader title={'Detil Transaksi'} left={['back']} />
        <ScrollView>
          {this.renderTransaction()}
          {this.renderPayment()}
          {this.renderPeserta()}
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    nav: state.nav,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    logout: (params) => dispatch(AuthActions.authSuccess('logout', params)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TransactionDetail);
