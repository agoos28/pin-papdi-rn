import React, {Component} from 'react';
import {RefreshControl, ScrollView, Text, View} from 'react-native';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Cart.style';
import {getValue} from '../../Helper/Validator';
import EventActions from '../../Redux/EventRedux';
import API from '../../Services/Api';
import {
  Card,
  Button,
  IconButton,
  Paragraph,
  Subheading,
} from 'react-native-paper';
import {formatMoney} from '../../Helper/Formater';
import MainHeader from '../../Components/MainHeader';

const api = API.create();

class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }
  async getCart() {
    const reqParams = {
      task: 'getCart',
      format: 'json',
    };
    const request = await api.event('cart', reqParams);
    const result = getValue(request, 'data', {default: []});

    if (typeof result === 'string') {
      return;
    }

    this.props.dispatch(EventActions.eventSuccess('cart', result));
    this.setState({cart: result});

    return;
  }

  async dellCart(id) {
    const reqParams = {
      id: id,
      useajax: 'true',
      task: 'dellCart',
    };

    const request = await api.event('cart', reqParams);
    const result = getValue(request, 'data', {default: null});

    if (typeof result === 'string') {
      return;
    }

    this.props.dispatch(EventActions.eventSuccess('cart', result));
    this.setState({cart: result});

    return;
  }

  renderCartItem(item, index) {
    return (
      <Card
        key={index}
        onPress={() =>
          this.props.navigation.navigate('CartDetail', {params: item})
        }>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
            paddingHorizontal: 16,
            borderBottomWidth: 0.5,
            paddingVertical: 16,
          }}>
          <View style={{flexGrow: 1, maxWidth: 200}}>
            <Subheading>{item.name}</Subheading>
            <Paragraph>{item.event_name}</Paragraph>
            <Paragraph>{item.title}</Paragraph>
          </View>
          <View style={{flexGrow: 0, width: 150}}>
            <Paragraph style={{textAlign: 'right', paddingVertical: 8}}>
              Rp.{formatMoney(item.price)}
            </Paragraph>
          </View>
          <View style={{flexGrow: 0, width: 34}}>
            <IconButton
              icon={'trash-can-outline'}
              size={20}
              onPress={() => this.dellCart(item.id)}
            />
          </View>
        </View>
      </Card>
    );
  }

  renderCardItems() {
    const {cart} = this.props;
    const cartEl = [];
    if (!cart || !cart.length) {
      return (
        <View
          style={{height: 300, alignItems: 'center', justifyContent: 'center'}}>
          <Paragraph>Keranjang Kosong</Paragraph>
        </View>
      );
    }
    cart.map((item, index) => {
      cartEl.push(this.renderCartItem(item, index));
    });
    return cartEl;
  }

  renderFooter() {
    const {cart} = this.props;
    let total = 0;
    if (!cart) {
      return null;
    }
    cart.map((item, index) => {
      total += parseInt(item.price);
    });

    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          paddingHorizontal: 16,
          paddingVertical: 16,
        }}>
        <View style={{flexGrow: 1}}>
          <Paragraph style={{paddingVertical: 8}}>
            TOTAL: Rp.{formatMoney(total)}
          </Paragraph>
        </View>
        <View style={{width: 130}}>
          <Button
            mode="contained"
            onPress={() => {
              if (this.props.cart && this.props.cart.length) {
                this.props.navigation.navigate('CartCheckout');
              }
            }}>
            CHECKOUT
          </Button>
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <MainHeader title={'Keranjang'} left={['back']} />
        <ScrollView
          style={{flexGrow: 1}}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this.getCart()}
            />
          }>
          {this.renderCardItems()}
        </ScrollView>
        <Card style={{height: 70}}>{this.renderFooter()}</Card>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {event} = state;
  return {
    cart: event.cart,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {dispatch};
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
