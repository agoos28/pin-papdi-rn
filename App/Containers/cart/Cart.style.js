import {StyleSheet} from 'react-native';
import {Colors, Metrics} from '../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.silver,
  },
  imagePickerToggle: {
    width: 60,
    height: 60,
    backgroundColor: '#fff',
    borderRadius: 40,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
  fileContainer: {
    flexDirection: 'row',
  },
  file: {
    width: 100,
    height: 100 * (800 / 600),
    marginRight: 10,
    marginBottom: 10,
  },
});
