import React, {Component} from 'react';
import {
  BackHandler,
  Image,
  PermissionsAndroid,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import BottomSheet from 'reanimated-bottom-sheet';
import {
  Button,
  Card,
  Paragraph,
  RadioButton,
  Subheading,
  TextInput,
  Title,
} from 'react-native-paper';
import MainHeader from '../../Components/MainHeader';
import {Tab, Tabs} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-crop-picker';
import styles from './Cart.style';
import {getValue} from '../../Helper/Validator';
import API from '../../Services/Api';

const api = API.create();

class CartCheckout extends Component {
  constructor(props) {
    super(props);
    const {user} = props;
    this.state = {
      tab: 0,
      name: user.name,
      email: user.email,
      phone: user.phone,
      address: user.address,
      country: user.country,
      city: user.city,
      province: user.province,
      postal: user.postal,
      paymentMethod: 'midtrans',
      bottomSheet: 0,
      orgName: user.org_name,
      orgAddress: user.org_address,
      files: [],
    };
  }

  componentDidMount() {
    if (Platform.OS === 'ios') {
      return;
    }
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.backHandle();
      return true;
    });
  }

  componentWillUnmount() {
    if (Platform.OS === 'ios') {
      return;
    }
    BackHandler.removeEventListener('hardwareBackPress', undefined);
  }

  backHandle() {
    const {tab} = this.state;
    if (tab > 0) {
      this.setState({tab: tab - 1});
    } else {
      this.props.navigation.goBack();
    }
  }

  isSponsor() {
    const {cart} = this.props;
    if (!cart || !cart[0]) {
      return false;
    }
    return cart[0].as === 'sponsor';
  }

  renderPemesan() {
    let complete = false;
    const isSponsor = this.isSponsor();
    const {
      name,
      email,
      phone,
      address,
      country,
      city,
      province,
      postal,
      orgName,
      orgAddress,
    } = this.state;
    if (!!phone && !!address && !!country && !!city && !!province && !!postal) {
      complete = true;
    }
    return (
      <ScrollView style={styles.container} keyboardShouldPersistTaps={'always'}>
        <Card style={{marginBottom: 8}}>
          <Card.Content>
            <Title style={{marginBottom: 10, fontSize: 18}}>Data Pemesan</Title>
            <TextInput
              label="Nama Lengkap"
              value={name}
              mode={'outlined'}
              style={{marginBottom: 6}}
              onChangeText={(val) => this.setState({name: val})}
              disabled={true}
            />
            <TextInput
              label="Alamat Email"
              value={email}
              mode={'outlined'}
              style={{marginBottom: 6}}
              onChangeText={(val) => this.setState({email: val})}
              disabled={true}
            />
            <TextInput
              label="Nomor Telepon"
              value={phone}
              mode={'outlined'}
              style={{marginBottom: 6}}
              onChangeText={(val) => this.setState({phone: val})}
            />
          </Card.Content>
        </Card>
        {isSponsor && (
          <Card style={{marginBottom: 8}}>
            <Card.Content>
              <Title style={{marginBottom: 10, fontSize: 18}}>Organisasi</Title>
              <TextInput
                label="Nama Organisasi"
                value={orgName}
                mode={'outlined'}
                style={{marginBottom: 6}}
                onChangeText={(val) => this.setState({orgName: val})}
              />
              <TextInput
                label="Alamat Organisasi"
                value={orgAddress}
                mode={'outlined'}
                style={{marginBottom: 6}}
                onChangeText={(val) => this.setState({orgAddress: val})}
              />
            </Card.Content>
          </Card>
        )}
        <Card>
          <Card.Content>
            <Title style={{marginBottom: 10, fontSize: 18}}>
              Alamat Penagihan
            </Title>
            <TextInput
              label="Alamat"
              value={address}
              mode={'outlined'}
              style={{marginBottom: 6}}
              onChangeText={(val) => this.setState({address: val})}
            />
            <TextInput
              label="Negara"
              value={country}
              mode={'outlined'}
              style={{marginBottom: 6}}
              onChangeText={(val) => this.setState({country: val})}
            />
            <TextInput
              label="Kota"
              value={city}
              mode={'outlined'}
              style={{marginBottom: 6}}
              onChangeText={(val) => this.setState({city: val})}
            />
            <TextInput
              label="Propinsi"
              value={province}
              mode={'outlined'}
              style={{marginBottom: 6}}
              onChangeText={(val) => this.setState({province: val})}
            />
            <TextInput
              label="Kode POS"
              value={postal}
              mode={'outlined'}
              style={{marginBottom: 6}}
              onChangeText={(val) => this.setState({postal: val})}
            />
          </Card.Content>
        </Card>
        <View style={{padding: 16}}>
          <Button
            disabled={!complete}
            mode="contained"
            onPress={() => this.setState({tab: 1})}>
            Pilih Metode Bayar
          </Button>
        </View>
      </ScrollView>
    );
  }

  renderMetodeBayar() {
    const {paymentMethod} = this.state;
    return (
      <ScrollView style={styles.container} keyboardShouldPersistTaps={'always'}>
        <Card style={{marginBottom: 8}}>
          <Card.Content>
            <Title style={{marginBottom: 10, fontSize: 18}}>
              Pilih metode pembayaran
            </Title>
            <RadioButton.Group
              onValueChange={(val) => this.setState({paymentMethod: val})}
              value={paymentMethod}>
              <RadioButton.Item label="Pembayaran Online" value="midtrans" />
              <RadioButton.Item label="Surat Komitmen" value="commitment" />
            </RadioButton.Group>
          </Card.Content>
        </Card>
        {paymentMethod === 'midtrans' ? (
          <Card style={{marginBottom: 8}}>
            <Card.Content>
              <Title style={{marginBottom: 10, fontSize: 18}}>
                Pembayaran Online
              </Title>
              <Paragraph style={{marginBottom: 10}}>
                Sistem pembayaran online dengan verifikasi otomatis yang
                terhubung dengan berbagai metode pembayaran di Indonesia.
              </Paragraph>
              <Subheading style={{fontWeight: '700'}}>Transfer Bank</Subheading>
              <Paragraph style={{marginBottom: 10}}>
                Pembayaran Transfer baik melalui ATM, mobile, atau internet
                banking. Batas waktu transfer adalah 1 x 24 jam dan pemesanan
                akan dibatalkan secara otomatis.
              </Paragraph>
              <Subheading style={{fontWeight: '700'}}>
                Debit Rekening
              </Subheading>
              <Paragraph style={{marginBottom: 10}}>
                Pembayaran melalui fasilitas Internet Banking dari berbagai Bank
                ternama.
              </Paragraph>
              <Subheading style={{fontWeight: '700'}}>
                kartu kredit/debit
              </Subheading>
              <Paragraph style={{marginBottom: 10}}>
                Pembayaran menggunakan kartu kredit/debit dari semua Bank yang
                berlogo VISA/MasterCard/JCB/Amex.
              </Paragraph>
            </Card.Content>
          </Card>
        ) : (
          <Card style={{marginBottom: 8}}>
            <Card.Content>
              <Title style={{marginBottom: 10, fontSize: 18}}>
                Upload dokumen komitmen
              </Title>
              <Paragraph>
                Verifikasi dengan mengirim surat komitmen. Maksimal 20 orang
                peserta
              </Paragraph>
              <Paragraph style={{marginBottom: 10}}>
                Sistem pembayaran menggunakan surat komitmen yang akan disetujui
                oleh admin.
              </Paragraph>
              <View style={styles.fileContainer}>
                {this.renderSelectedFile()}
                <TouchableOpacity
                  style={{
                    ...styles.file,
                    backgroundColor: '#ccc',
                    borderRadius: 5,
                    justifyContent: 'center',
                  }}
                  onPress={() => this.bottomSheetToggle(1)}>
                  <Subheading style={{textAlign: 'center'}}>
                    Pilih File
                  </Subheading>
                </TouchableOpacity>
              </View>
            </Card.Content>
          </Card>
        )}
        <View style={{padding: 16}}>
          <Button
            mode="contained"
            style={{marginBottom: 8}}
            onPress={() => this.checkOut()}>
            {paymentMethod === 'midtrans'
              ? 'Ke Halaman Pembayaran'
              : 'Kirim Dokumen'}
          </Button>
          <Button onPress={() => this.backHandle()}>Kembali</Button>
        </View>
      </ScrollView>
    );
  }

  async openImagePicker(useCamera = false) {
    const params = {
      mediaType: 'photo',
      width: 600,
      height: 800,
      cropping: true,
      cropperCircleOverlay: false,
      compressImageMaxWidth: 600,
      compressImageMaxHeight: 800,
    };
    try {
      if (useCamera === true) {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'App Camera Permission',
            message:
              'App needs access to your camera ' +
              'so you can take awesome pictures.',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          ImagePicker.openCamera(params).then((image) => {
            this.selecFile({
              name: image.path.split('/').pop(),
              uri: image.path,
              type: image.mime,
            });
          });
        } else {
        }
      } else {
        ImagePicker.openPicker(params).then((image) => {
          this.selecFile({
            uri: image.path,
            type: image.mime,
            name: image.path.split('/').pop(),
          });
        });
      }
    } catch (error) {
      console.log(error);
    }
    return;
  }

  renderImagePicker() {
    return (
      <View
        style={{
          paddingTop: 30,
          paddingHorizontal: 3,
          height: 400,
          backgroundColor: '#a65381',
          elevation: 30,
          borderTopLeftRadius: 10,
          borderTopRightRadius: 10,
          marginHorizontal: 5,
        }}>
        <Title style={{textAlign: 'center', color: '#fff'}}>Pilih Metode</Title>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'flex-start',
            paddingHorizontal: 30,
            paddingVertical: 30,
          }}>
          <TouchableOpacity
            style={{alignItems: 'center', margin: 16}}
            activeOpacity={0.8}
            onPress={() => this.openImagePicker(true)}>
            <View style={styles.imagePickerToggle}>
              <Icon name={'camera'} size={24} color={'#A65381'} />
            </View>
            <Text style={{color: '#fff'}}>Kamera</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{alignItems: 'center', margin: 16}}
            activeOpacity={0.8}
            onPress={() => this.openImagePicker(false)}>
            <View style={styles.imagePickerToggle}>
              <Icon name={'photo'} size={24} color={'#A65381'} />
            </View>
            <Text style={{color: '#fff'}}>Gallery</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{alignItems: 'center', margin: 16}}
            activeOpacity={0.8}
            onPress={() => this.openImagePicker(false)}>
            <View style={styles.imagePickerToggle}>
              <Icon name={'file'} size={24} color={'#A65381'} />
            </View>
            <Text style={{color: '#fff'}}>File</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  selecFile(file) {
    this.setState({
      files: [...this.state.files, file],
    });
    this.bottomSheetToggle(0);
  }

  async uploadCommitmentLetter() {
    const {files} = this.state;
    const reqParams = {
      task: 'uploadImage',
      type: 'commitment',
    };

    files.map((item, index) => {
      reqParams['file[' + index + ']'] = item;
    });

    const request = await api.event('cart', reqParams);
    const result = getValue(request, 'data', {default: null});
    return result;
  }

  async checkOut() {
    const {
      name,
      email,
      phone,
      address,
      country,
      city,
      province,
      postal,
      paymentMethod,
    } = this.state;

    const reqParams = {
      id: this.props.user.id,
      name: name,
      email: email,
      phone: phone,
      address: address,
      country: country,
      city: city,
      province: province,
      postal: postal,
      payment_method: paymentMethod,
      task: 'checkoutCart',
      ajax: 1,
    };

    if (paymentMethod === 'commitment') {
      reqParams.description = await this.uploadCommitmentLetter();
      if (!reqParams.description) {
        alert('Gagal Upload File');
        return;
      }
    }

    const request = await api.event('cart', reqParams);
    const result = getValue(request, 'data', {default: null});

    if (result) {
      if (paymentMethod === 'commitment') {
        this.props.navigation.navigate('Transaction');
      }
      if (paymentMethod === 'midtrans') {
        this.setState({
          transactionId: result.id,
          snapToken: result.token,
        });
        this.props.navigation.navigate('SnapPayment', {
          params: {token: result.token, transactionId: result.id},
        });
      }
    }
  }

  bottomSheetToggle(index) {
    this.bottomSheet.snapTo(index);
    this.setState({
      bottomSheet: index,
    });
  }

  removeFile(index) {
    const {files} = this.state;
    const newFiles = [];
    files.map((item, _index) => {
      if (index !== _index) {
        newFiles.push(item);
      }
    });
    this.setState({
      files: newFiles,
    });
  }

  renderSelectedFile() {
    const {files} = this.state;
    if (!files.length) {
      return null;
    }
    return files.map((item, index) => {
      return (
        <TouchableOpacity
          key={index}
          onLongPress={() => this.removeFile(index)}>
          <Image style={styles.file} source={{uri: item.uri}} />
        </TouchableOpacity>
      );
    });
  }

  render() {
    const {cart} = this.props;
    if (!cart || !cart.length) {
      this.props.navigation.replace('Cart');
      return null;
    }
    const {tab} = this.state;
    const tabStyle = {
      activeTabStyle: {backgroundColor: '#a65381'},
      tabStyle: {backgroundColor: '#a65381'},
      textStyle: {color: '#fff', fontSize: 13},
      activeTextStyle: {color: '#fff', fontSize: 13},
      disabled: true,
    };

    return (
      <View style={{flex: 1}}>
        <MainHeader
          left={[{type: 'customBack', action: () => this.backHandle()}]}
          right={['cart']}
          title={'Registrasi Event'}
          cart={this.props.cart}
        />
        <Tabs
          style={{backgroundColor: '#a65381'}}
          tabBarBackgroundColor={'#a65381'}
          tabBarUnderlineStyle={{backgroundColor: '#e3e3e3'}}
          locked={true}
          initialPage={0}
          onChangeTab={(tab) => {
            this.setState({tab: tab.i});
          }}
          page={tab}>
          <Tab {...tabStyle} heading="Informasi Pemesan">
            {this.renderPemesan()}
          </Tab>
          <Tab {...tabStyle} heading="Metode Pembayaran">
            {this.renderMetodeBayar()}
          </Tab>
        </Tabs>
        <TouchableWithoutFeedback onPress={() => this.bottomSheetToggle(0)}>
          <View
            style={{
              position: 'absolute',
              top: this.state.bottomSheet > 0 ? 0 : undefined,
              left: 0,
              right: 0,
              bottom: 0,
              zIndex: 99,
            }}
          />
        </TouchableWithoutFeedback>
        <BottomSheet
          ref={(th) => (this.bottomSheet = th)}
          overdragResistanceFactor={1}
          enabledContentTapInteraction={false}
          enabledInnerScrolling={false}
          snapPoints={[0, 250]}
          borderRadius={10}
          renderContent={() => this.renderImagePicker()}
          onCloseEnd={() => {
            this.bottomSheetToggle(0);
          }}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
    cart: state.event.cart,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {dispatch};
};

export default connect(mapStateToProps, mapDispatchToProps)(CartCheckout);
