import React, {Component} from 'react';
import {ScrollView, View} from 'react-native';
import {connect} from 'react-redux';
import styles from './Cart.style';
import {Card, Divider, Paragraph, Subheading} from 'react-native-paper';
import {Col, Grid} from 'react-native-easy-grid';
import MainHeader from '../../Components/MainHeader';

class CartDetail extends Component {
  constructor(props) {
    super(props);
    const navigationParams = props.navigation.getParam('params', null);
    this.state = {
      detail: navigationParams,
    };
  }

  render() {
    const {name, email, phone, npa, bidang, workshops} = this.state.detail;
    return (
      <View style={styles.container}>
        <MainHeader title={'Detil Keranjang'} left={['back']} />
        <ScrollView>
          <Card style={{marginBottom: 8}}>
            <Card.Content>
              <Grid>
                <Col style={{paddingVertical: 8}}>
                  <Subheading>Nama</Subheading>
                </Col>
                <Col style={{paddingVertical: 8}}>
                  <Subheading style={{textAlign: 'right'}}>{name}</Subheading>
                </Col>
              </Grid>
              <Divider />
              <Grid>
                <Col style={{paddingVertical: 8}}>
                  <Subheading>Email</Subheading>
                </Col>
                <Col style={{paddingVertical: 8}}>
                  <Subheading style={{textAlign: 'right'}}>{email}</Subheading>
                </Col>
              </Grid>
              <Divider />
              <Grid>
                <Col style={{paddingVertical: 8}}>
                  <Subheading>Telepon</Subheading>
                </Col>
                <Col style={{paddingVertical: 8}}>
                  <Subheading style={{textAlign: 'right'}}>{phone}</Subheading>
                </Col>
              </Grid>
              <Divider />
              <Grid>
                <Col style={{paddingVertical: 8}}>
                  <Subheading>Keanggotaan</Subheading>
                </Col>
                <Col style={{paddingVertical: 8}}>
                  <Subheading style={{textAlign: 'right'}}>
                    {npa ? 'Anggota' : 'Non-anggota'}
                  </Subheading>
                </Col>
              </Grid>
              <Divider />
              <Grid>
                <Col style={{paddingVertical: 8}}>
                  <Subheading>Bidang</Subheading>
                </Col>
                <Col style={{paddingVertical: 8}}>
                  <Subheading style={{textAlign: 'right'}}>{bidang}</Subheading>
                </Col>
              </Grid>
              <Divider />
              <Subheading style={{paddingVertical: 8}}>
                Pilihan Workshop
              </Subheading>
              {workshops &&
                workshops.map((item, index) => (
                  <Grid key={index}>
                    <Col style={{width: 20}}>
                      <Paragraph>{index + 1}</Paragraph>
                    </Col>
                    <Col>
                      <Paragraph>{item.name}</Paragraph>
                    </Col>
                  </Grid>
                ))}
            </Card.Content>
          </Card>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(CartDetail);
