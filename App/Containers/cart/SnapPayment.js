import React, {Component} from 'react';
import {connect} from 'react-redux';
import WebView from 'react-native-webview';
import SafeAreaView from 'react-native-safe-area-view';
import {Platform, StatusBar} from 'react-native';
import theme from '../../Themes/PaperTheme';
import {NavigationActions} from 'react-navigation';
import EventActions from '../../Redux/EventRedux';

class SnapPayment extends Component {
  constructor(props) {
    super(props);
    const navigationParams = props.navigation.getParam('params', null);
    this.state = {
      transactionId: navigationParams.transactionId,
      token: navigationParams.token,
    };
  }
  redirect() {
    if (!this.state.transactionId) {
      this.props.navigation.goBack();
    } else {
      this.props.dispatch(EventActions.eventSuccess('cart', null));
      this.props.navigation.reset(
        [
          NavigationActions.navigate({
            routeName: 'Home',
          }),
          NavigationActions.navigate({
            routeName: 'TransactionDetail',
            params: {
              params: this.state.transactionId,
            },
          }),
        ],
        1,
      );
    }
  }
  render() {
    const {token, transactionId} = this.state;
    const html =
      `
      <html class="wf-active">
        <head>
          <meta charset="utf-8"/>
          <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
          <script src="https://app.midtrans.com/snap/snap.js" data-client-key="Mid-client-tbjm_9qn4fuYprzI"></script>
          <script>
              document.addEventListener('DOMContentLoaded', function(){
                function callBack(action, data){window.ReactNativeWebView.postMessage(JSON.stringify({action: action, data: data}))}
                snap.pay('` +
      token +
      `', {
                  onClose: function(data){callBack('close', data)},
                  onSuccess: function(data){callBack('success', data)},
                  onPending: function(data){callBack('pending', data)},
                  onError: function(data){callBack('error', data)}
                })}, false);
          </script>
        </head>
        <body></body>
      </html>`;
    return (
      <SafeAreaView style={{flex: 1}}>
        <StatusBar
          animated={true}
          barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'}
          backgroundColor={theme.colors.primary}
        />
        <WebView
          source={{html: html}}
          originWhitelist={['https://*']}
          javaScriptCanOpenWindowsAutomatically={true}
          onMessage={(event) => {
            const message = JSON.parse(event.nativeEvent.data);
            switch (message.action) {
              case 'close':
                this.redirect();
                break;
              case 'success':
                this.redirect();
                break;
              case 'pending':
                this.redirect();
                break;
              case 'error':
                alert(message.data.status_message[0]);
                break;
            }
          }}
          onNavigationStateChange={(navState) => {
            // Keep track of going back navigation within component
            // console.log(navState);
          }}
        />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {dispatch};
};

export default connect(mapStateToProps, mapDispatchToProps)(SnapPayment);
