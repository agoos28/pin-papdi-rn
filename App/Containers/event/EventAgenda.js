import React, {Component} from 'react';
import {View, Dimensions} from 'react-native';
import {connect} from 'react-redux';
import MainHeader from '../../Components/MainHeader';
import {TabView, TabBar} from 'react-native-tab-view';
import EventAgendaItem from './EventAgendaItem';

class EventAgenda extends Component {
  constructor(props) {
    super(props);
    const navigationParams = props.navigation.getParam('params', null);
    const tabs = [];
    navigationParams.map((item, index) => {
      tabs.push({
        key: item.id,
        title: item.title,
      });
    });
    this.state = {
      agendas: navigationParams,
      index: 0,
      routes: tabs,
    };
  }

  renderScene = ({route, jumpTo}) => {
    const {agendas} = this.state;
    const agenda = agendas.filter((item) => item.id === route.key);
    return (
      <EventAgendaItem {...this.props} jumpTo={jumpTo} agenda={agenda[0]} />
    );
  };

  renderTabBar = (props) => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={{backgroundColor: '#f55753', height: 3}}
      tabStyle={{width: Dimensions.get('window').width / 3}}
      labelStyle={{color: '#333', fontWeight: 'bold'}}
      style={{backgroundColor: '#f9d975'}}
    />
  );

  render() {
    const {index, routes} = this.state;
    return (
      <View style={{flex: 1}}>
        <MainHeader left={['back']} title={'Agenda Kegiatan'} />
        <TabView
          ovescroll
          scrollEnabled
          navigationState={{index, routes}}
          onIndexChange={(i) => this.setState({index: i})}
          renderScene={this.renderScene}
          labelStyle={{width: 200}}
          initialLayout={{width: Dimensions.get('window').width}}
          renderTabBar={this.renderTabBar}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(EventAgenda);
