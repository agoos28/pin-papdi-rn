import React from 'react';
import {ScrollView, Text, KeyboardAvoidingView, View} from 'react-native';

import styles from './Event.style';
import {
  Card,
  DataTable,
  Divider,
  Paragraph,
  Subheading,
  Title,
} from 'react-native-paper';
import HTML from 'react-native-render-html';
import {formatDate, formatTime} from '../../Helper/Formater';

export default class EventAgendaItem extends React.PureComponent {
  renderAgendaItem(timeline) {
    const res = [];
    timeline.map((item, index) => {
      res.push(
        <View
          key={index}
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            paddingLeft: 8,
            borderBottomWidth: timeline.length < index ? 0 : 0.5,
            borderColor: '#cfcfcfb3',
          }}>
          <View style={{flexGrow: 0}}>
            <Paragraph style={{paddingVertical: 10, paddingHorizontal: 10}}>
              {formatTime(item.start_time)} - {formatTime(item.end_time)}
            </Paragraph>
          </View>
          {item.activity_type === '2' ? (
            this.renderWorkshopItem(item)
          ) : (
            <View
              style={{
                flex: 1,
                flexGrow: 1,
                borderLeftWidth: 0.5,
                borderColor: '#cfcfcfb3',
              }}>
              <Paragraph
                style={{
                  flex: 1,
                  flexWrap: 'wrap',
                  paddingVertical: 10,
                  paddingHorizontal: 8,
                }}>
                {item.name}
              </Paragraph>
            </View>
          )}
        </View>,
      );
    });
    return res;
  }

  renderWorkshopItem(timeline) {
    const {activities} = timeline;
    const res = [];
    activities.map((item, index) => {
      res.push(
        <View
          key={index}
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-start',
            paddingHorizontal: 8,
            borderBottomWidth: 0.5,
            paddingVertical: 5,
            borderColor: '#cfcfcfb3',
            backgroundColor: index % 2 ? '#f7f7f7' : '#fffff',
          }}>
          <View style={{flexGrow: 0, width: 65}}>
            <Paragraph>{item.code}</Paragraph>
          </View>
          <View style={{flex: 1, flexGrow: 1}}>
            <Paragraph>{item.name}</Paragraph>
          </View>
        </View>,
      );
    });
    return (
      <View
        style={{
          flex: 1,
          flexGrow: 1,
          borderLeftWidth: 0.5,
          borderColor: '#e6e6e6b3',
        }}>
        <View style={{backgroundColor: '#40d9ca', paddingVertical: 5}}>
          <Paragraph style={{textAlign: 'center'}}>
            {!!timeline.name && timeline.name.replace('&amp;', '&')}
          </Paragraph>
        </View>
        {res}
      </View>
    );
  }

  render() {
    const agenda = this.props.agenda;
    return (
      <View style={styles.container}>
        <View
          style={{
            paddingHorizontal: 16,
            paddingVertical: 10,
            elevation: 2,
            backgroundColor: '#f7f7f7',
          }}>
          <Title>{agenda.title}</Title>
          <Paragraph>{agenda.description}</Paragraph>
        </View>
        <ScrollView style={styles.container}>
          <Card>{this.renderAgendaItem(agenda.timeline)}</Card>
        </ScrollView>
      </View>
    );
  }
}
