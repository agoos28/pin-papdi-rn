import React, {Component} from 'react';
import {ScrollView, View} from 'react-native';
import {connect} from 'react-redux';
import styles from './Event.style';
import MainHeader from '../../Components/MainHeader';
import {Card} from 'react-native-paper';
import HTML from 'react-native-render-html';

class EventContent extends Component {
  constructor(props) {
    super(props);
    const navigationParams = props.navigation.getParam('params', null);
    this.state = {
      expanded: null,
      content: navigationParams,
    };
  }

  render() {
    const {content} = this.state;
    return (
      <View style={{flex: 1}}>
        <MainHeader left={['back']} title={content.title} />
        <ScrollView style={styles.container}>
          <Card>
            <Card.Content>
              <HTML
                tagsStyles={{
                  p: {margin: 0, marginBottom: 15},
                  ol: {marginTop: 15},
                }}
                html={content.content?.replace('bolder', 'bold') || ''}
                ignoredStyles={['font-family', 'background-color']}
              />
            </Card.Content>
          </Card>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(EventContent);
