import React, {Component} from 'react';
import {ScrollView, View} from 'react-native';
import {connect} from 'react-redux';
import styles from './Event.style';
import MainHeader from '../../Components/MainHeader';
import {getValue} from '../../Helper/Validator';
import API from '../../Services/Api';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import EmptyContent from '../../Components/EmptyContent';
import {Metrics} from '../../Themes';
import {Image} from 'react-native-animatable';
import {
  Button,
  Card,
  Divider,
  List,
  Paragraph,
  Title,
} from 'react-native-paper';

const api = API.create();

class EventDetail extends Component {
  constructor(props) {
    super(props);
    const navigationParams = props.navigation.getParam('params', null);
    this.state = {
      params: navigationParams,
      event: null,
      loading: false,
      refreshing: false,
    };
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
    this.setState({
      loading: true,
      refreshing: !!this.state.detail,
    });
    const reqParams = {
      id: this.state.params.id,
      catId: this.state.params.catId,
    };
    const request = await api.event('getDetail', reqParams);
    const result = getValue(request, 'data.data', {default: []});
    this.setState({
      event: result,
      loading: false,
      refreshing: false,
    });
  }

  viewPackage() {
    const {event} = this.state;
    this.props.navigation.navigate('EventPackages', {params: event.packages});
  }

  viewAgenda() {
    const {event} = this.state;
    this.props.navigation.navigate('EventAgenda', {params: event.agenda});
  }

  viewWorkshops() {
    const {event} = this.state;
    this.props.navigation.navigate('EventWorkshops', {
      params: event.workshops,
    });
  }

  viewPengantar() {
    const {event} = this.state;
    this.props.navigation.navigate('EventContent', {
      params: {title: 'Pengantar', content: event.pengantar},
    });
  }

  viewInfo() {
    const {event} = this.state;
    this.props.navigation.navigate('EventContent', {
      params: {
        title: 'Info Kegiatan',
        content: event.info,
      },
    });
  }

  viewKetentuan() {
    const {event} = this.state;
    this.props.navigation.navigate('EventContent', {
      params: {title: 'Ketentuan Pendaftaran', content: event.ketentuan},
    });
  }

  renderEmpty = () => {
    const {loading} = this.state;
    if (loading) {
      return (
        <SkeletonContent
          containerStyle={{flex: 1, width: Metrics.screenWidth}}
          isLoading
          layout={[
            {
              key: 'som',
              width: Metrics.screenWidth,
              height: 220,
              marginBottom: 6,
            },
            {
              key: 'someId',
              width: Metrics.screenWidth,
              height: 20,
              marginBottom: 6,
            },
            {
              key: 'someOtherId',
              width: Metrics.screenWidth,
              height: 20,
              marginBottom: 6,
            },
          ]}
        />
      );
    }
    return <EmptyContent textDescription={'Produk tidak ditemukan'} />;
  };

  renderContent() {
    const {event} = this.state;
    if (!event) {
      return this.renderEmpty();
    }
    return (
      <React.Fragment>
        <Image
          source={{uri: event.image}}
          style={{
            width: Metrics.screenWidth,
            height: Metrics.screenWidth * (1280 / 902),
          }}
        />
        <Card style={{marginBottom: 0}}>
          <Card.Content>
            <Title>{event.title}</Title>
            <Paragraph>{event.description}</Paragraph>
          </Card.Content>
          <List.Section>
            <Divider />
            <List.Item
              title={'Pengantar'}
              right={() => <List.Icon icon="chevron-right" />}
              onPress={() => this.viewPengantar()}
            />
            <Divider />
            <List.Item
              title={'Info Kegiatan'}
              right={() => <List.Icon icon="chevron-right" />}
              onPress={() => this.viewInfo()}
            />
            <Divider />
            <List.Item
              title={'Paket Pendaftaran'}
              right={() => <List.Icon icon="chevron-right" />}
              onPress={() => this.viewPackage()}
            />
            <Divider />
            <List.Item
              title={'Agenda Kegiatan'}
              right={() => <List.Icon icon="chevron-right" />}
              onPress={() => this.viewAgenda()}
            />
            <Divider />
            <List.Item
              title={'Workshop'}
              right={() => <List.Icon icon="chevron-right" />}
              onPress={() => this.viewWorkshops()}
            />
            <Divider />
            <List.Item
              title={'Ketentuan Pendaftaran'}
              right={() => <List.Icon icon="chevron-right" />}
              onPress={() => this.viewKetentuan()}
            />
          </List.Section>
        </Card>
        <View style={{padding: 5}}>
          {this.props.auth.guest ? (
            <Button
              mode={'contained'}
              onPress={() => this.props.navigation.push('Login')}>
              Login Untuk Daftar
            </Button>
          ) : (
            <Button
              mode={'contained'}
              onPress={() =>
                this.props.navigation.navigate('EventRegister', {params: event})
              }>
              Daftar Sekarang
            </Button>
          )}
        </View>
      </React.Fragment>
    );
  }

  render() {
    return (
      <React.Fragment>
        <MainHeader left={['back']} title={this.state.params.title} />
        <ScrollView style={styles.container}>{this.renderContent()}</ScrollView>
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({auth}) => {
  return {auth};
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(EventDetail);
