import React from 'react';
import {ScrollView, Text, KeyboardAvoidingView, View} from 'react-native';

import styles from './Event.style';
import {
  Card,
  DataTable,
  Divider,
  Paragraph,
  Subheading,
  Title,
} from 'react-native-paper';
import HTML from 'react-native-render-html';
import {formatDate, formatMoney} from '../../Helper/Formater';

export default class EventPackageItem extends React.PureComponent {
  renderPackageItem(pack) {
    const res = [];
    pack.map((item, index) => {
      res.push(
        <Card key={index} style={{marginBottom: 10}}>
          <Card.Title title={item.title} subtitle={item.description} />
          <Divider />
          <DataTable>
            {item.price_member !== '0' && (
              <DataTable.Row>
                <DataTable.Cell>Anggota PAPDI</DataTable.Cell>
                <DataTable.Cell>
                  Rp.{formatMoney(item.price_member)}
                </DataTable.Cell>
              </DataTable.Row>
            )}
            {item.price_finasim !== '0' && (
              <DataTable.Row>
                <DataTable.Cell>Anggota PAPDI Finasim</DataTable.Cell>
                <DataTable.Cell>
                  Rp.{formatMoney(item.price_finasim)}
                </DataTable.Cell>
              </DataTable.Row>
            )}
            {item.price !== '0' && (
              <DataTable.Row>
                <DataTable.Cell>Spesialis Lainnya</DataTable.Cell>
                <DataTable.Cell>Rp.{formatMoney(item.price)}</DataTable.Cell>
              </DataTable.Row>
            )}
            {item.price_dokter_umum !== '0' && (
              <DataTable.Row>
                <DataTable.Cell>Dokter Umum</DataTable.Cell>
                <DataTable.Cell>
                  Rp.{formatMoney(item.price_dokter_umum)}
                </DataTable.Cell>
              </DataTable.Row>
            )}
          </DataTable>
        </Card>,
      );
    });
    return res;
  }
  render() {
    const pack = this.props.package;
    return (
      <ScrollView style={styles.container}>
        <View style={{paddingHorizontal: 16, paddingVertical: 10}}>
          <Title>{pack.title}</Title>
        </View>
        {this.renderPackageItem(pack.items)}
        <Card>
          <Card.Content style={{paddingLeft: 15, paddingTop: 20}}>
            <HTML
              tagsStyles={{p: {margin: 0, marginBottom: 8}}}
              html={pack.description}
            />
          </Card.Content>
        </Card>
      </ScrollView>
    );
  }
}
