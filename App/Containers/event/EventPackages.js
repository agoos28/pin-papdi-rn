import React, {Component} from 'react';
import {KeyboardAvoidingView, ScrollView, View} from 'react-native';
import {connect} from 'react-redux';
import styles from './Event.style';
import MainHeader from '../../Components/MainHeader';
import {getValue} from '../../Helper/Validator';
import API from '../../Services/Api';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import EmptyContent from '../../Components/EmptyContent';
import {Metrics} from '../../Themes';
import {Image} from 'react-native-animatable';
import {BottomNavigation, Text} from 'react-native-paper';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import EventDetailPengantarScreen from './EventDetailPengantarScreen';
import {Dimensions} from 'react-native';
import EventPackageItem from './EventPackageItem';

class EventPackages extends Component {
  constructor(props) {
    super(props);
    const navigationParams = props.navigation.getParam('params', null);
    const tabs = [];
    navigationParams.map((item, index) => {
      tabs.push({
        key: item.id,
        title: item.title,
      });
    });
    this.state = {
      packages: navigationParams,
      index: 0,
      routes: tabs,
    };
  }

  renderScene = ({route, jumpTo}) => {
    const {packages} = this.state;
    const pack = packages.filter((item) => item.id === route.key);
    return <EventPackageItem {...this.props} package={pack[0]} />;
  };

  renderTabBar = (props) => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={{backgroundColor: '#fff', height: 3}}
      tabStyle={{width: Dimensions.get('window').width / 3}}
      labelStyle={{color: '#fff', fontWeight: 'bold'}}
      style={{backgroundColor: '#10cfbd'}}
    />
  );

  render() {
    const {index, routes} = this.state;
    return (
      <View style={{flex: 1}}>
        <MainHeader left={['back']} title={'Paket Pendaftaran'} />
        <TabView
          ovescroll
          scrollEnabled
          navigationState={{index, routes}}
          onIndexChange={(i) => this.setState({index: i})}
          renderScene={this.renderScene}
          labelStyle={{width: 200}}
          initialLayout={{width: Dimensions.get('window').width}}
          renderTabBar={routes.length > 1 ? this.renderTabBar : () => null}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(EventPackages);
