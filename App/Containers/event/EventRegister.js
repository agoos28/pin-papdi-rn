import React, {Component} from 'react';
import {
  Alert,
  BackHandler,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import styles from './Event.style';
import MainHeader from '../../Components/MainHeader';
import {
  ActivityIndicator,
  Button,
  Caption,
  Card,
  Divider,
  Paragraph,
  RadioButton,
  Subheading,
  TextInput,
  Title,
} from 'react-native-paper';
import {Picker, Tab, Tabs} from 'native-base';
import {Icon} from 'react-native-vector-icons/FontAwesome';
import {Grid, Col} from 'react-native-easy-grid';
import {formatMoney} from '../../Helper/Formater';
import API from '../../Services/Api';
import {getValue} from '../../Helper/Validator';
import EventActions from '../../Redux/EventRedux';
import TextInputMask from 'react-native-text-input-mask';

const api = API.create();

class EventRegister extends Component {
  constructor(props) {
    super(props);
    const navigationParams = props.navigation.getParam('params', null);
    this.maxOption = 8;
    this.state = {
      tab: 0,
      event: navigationParams,
      as: 'self',
      name: '',
      keanggotaan: 'anggota',
      membership: '',
      npa: '',
      spesialis: '',
      bidang: '',
      workshops: false,
      selectedPackage: null,
      workshopOption: [],
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.tab !== prevState.tab) {
      Keyboard.dismiss();
    }
  }

  componentDidMount() {
    if (Platform.OS === 'ios') {
      return;
    }
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.backHandle();
      return true;
    });
  }

  componentWillUnmount() {
    if (Platform.OS === 'ios') {
      return;
    }
    BackHandler.removeEventListener('hardwareBackPress', undefined);
  }

  async checkNpa() {
    const {npa} = this.state;
    if (!npa) {
      // console.log('error');
    }
    const reqParams = {
      task: 'checkNpa',
      npa: npa,
    };
    const request = await api.event('register', reqParams);
    const result = getValue(request, 'data', {default: false});
    // log(result);
    if (!result) {
      Alert.alert('Nomor NPA tidak valid');
    } else {
      this.setState({
        membership: result.name.indexOf('FINASIM') >= 0 ? 'finasim' : 'anggota',
        name: result.name,
        email: result.email[0],
        tab: 1,
      });
    }
  }

  async processMembership() {
    const {keanggotaan, bidang, name} = this.state;
    switch (keanggotaan) {
      case 'anggota':
        await this.checkNpa();
        break;
      case 'non-anggota':
        this.setState({
          membership: bidang,
          name: name,
          tab: 1,
        });
        break;
    }
  }

  async getCart() {
    const reqParams = {
      task: 'getCart',
      format: 'json',
    };
    const request = await api.event('cart', reqParams);
    const result = getValue(request, 'data', {default: null});

    if (result) {
      this.props.dispatch(EventActions.eventSuccess('cart', result));
      this.setState({cart: result});
    }
  }

  async getWorkshopOptions(id) {
    this.setState({workshops: null, workshopOption: null});
    const reqParams = {
      task: 'getPackageOptions',
      id: id,
    };
    const request = await api.event('register', reqParams);
    const result = getValue(request, 'data', {default: false});

    if (!result) {
      Alert.alert('Workshops tidak ditemukan');
    } else {
      this.setState({workshops: result});
    }
  }

  async processPackage(item) {
    this.setState({
      tab: 2,
      selectedPackage: item,
    });
    await this.getWorkshopOptions(item.id);
    await this.getCart();
  }

  getAvailable(option) {
    const {cart} = this.state;
    var seat = option.seats - option.registered;
    if (!cart) {
      return seat;
    }
    cart.map(function (item) {
      item.workshops.map(function (_item) {
        if (!!_item && _item.id === option.id) {
          seat = seat - 1;
        }
      });
    });

    return seat;
  }

  handleSelectWorkshop(item, _item) {
    const {workshopOption} = this.state;
    if (
      !!workshopOption &&
      !!workshopOption[item.group.id] &&
      workshopOption[item.group.id].id === _item.id
    ) {
      const _workshopOption = {...workshopOption};
      delete _workshopOption[item.group.id];
      this.setState({
        workshopOption: _workshopOption,
      });
    } else {
      if (
        !!workshopOption &&
        this.maxOption <= Object.keys(workshopOption).length
      ) {
        Alert.alert(
          'Maximum',
          'Anda hanya diperbolehkan memilih ' + this.maxOption + ' workshop',
        );
        return;
      }
      this.setState({
        workshopOption: {
          ...workshopOption,
          [item.group.id]: _item,
        },
      });
    }
  }

  renderWorkshops() {
    const {workshops, workshopOption} = this.state;
    let requireOption = 0;
    let maxOption = 8;
    // console.log(workshopOption);
    if (!workshops) {
      return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <ActivityIndicator animating />
        </View>
      );
    }
    // console.log(workshops);
    const el = [];
    const opts = workshops.options.sort((a, b) =>
      a.group.name.toUpperCase() < b.group.name.toUpperCase()
        ? -1
        : a.group.name.toUpperCase() > b.group.name.toUpperCase()
        ? 1
        : 0,
    );
    opts.map((item, index) => {
      let haveSelection = false;
      let canSelect = false;
      const _el = [];
      item.selections.map((_item, _index) => {
        const available = this.getAvailable(_item);
        haveSelection = available > 0;
        if (!!haveSelection && !canSelect) {
          canSelect = true;
        }
        _el.push(
          <View>
            <Divider />
            <Grid
              style={{
                alignItems: 'center',
                paddingHorizontal: 16,

                backgroundColor:
                  (workshopOption &&
                    workshopOption[item.group.id] &&
                    workshopOption[item.group.id].id) === _item.id
                    ? '#fff0f8'
                    : undefined,
              }}
              onPress={() =>
                haveSelection && this.handleSelectWorkshop(item, _item)
              }>
              <Col size={3} style={{paddingVertical: 6}}>
                <Paragraph>{_item.name}</Paragraph>
              </Col>
              <Col size={1} style={{paddingVertical: 6}}>
                <Paragraph style={{textAlign: 'center'}}>
                  {haveSelection ? available : 'Habis'}
                </Paragraph>
              </Col>
              <Col style={{width: 40}}>
                <RadioButton
                  value={_item.id}
                  onPress={() =>
                    haveSelection && this.handleSelectWorkshop(item, _item)
                  }
                />
              </Col>
            </Grid>
          </View>,
        );
      });
      if (canSelect) {
        if (this.maxOption > requireOption) {
          requireOption++;
        }
      }
      el.push(
        <Card key={index} style={{marginBottom: 8}}>
          <View style={{paddingHorizontal: 16}}>
            <Paragraph
              style={{fontWeight: 'bold', marginBottom: 0, paddingVertical: 6}}>
              {item.group.name}
            </Paragraph>
            <Divider />
            <Grid>
              <Col size={3} style={{paddingVertical: 6}}>
                <Caption>{item.group.date}</Caption>
              </Col>
              <Col size={1} style={{paddingVertical: 6}}>
                <Caption style={{textAlign: 'center'}}>Tersedia</Caption>
              </Col>
              <Col style={{width: 40, paddingVertical: 6}}>
                <Caption style={{textAlign: 'center'}}>Pilih</Caption>
              </Col>
            </Grid>
          </View>

          <RadioButton.Group
            style={{paddingHorizontal: 16}}
            value={
              workshopOption && workshopOption[item.group.id]
                ? workshopOption[item.group.id].id
                : null
            }>
            {_el}
          </RadioButton.Group>
        </Card>,
      );
    });
    return (
      <View style={{flex: 1}}>
        <View style={{height: 50, paddingHorizontal: 16, paddingTop: 8}}>
          <Grid>
            <Col>
              <Subheading style={{paddingVertical: 4}}>
                Pilih workshop yang akan diikuti
              </Subheading>
            </Col>
            <Col style={{width: 60}}>
              <Title style={{textAlign: 'center'}}>
                {workshopOption ? Object.keys(workshopOption).length : 0}/
                {requireOption}
              </Title>
            </Col>
          </Grid>
        </View>
        <ScrollView
          style={styles.container}
          keyboardShouldPersistTaps={'always'}>
          <View style={[styles.container, {paddingTop: 8}]}>
            {el}
            <View style={{padding: 16}}>
              <Button
                disabled={
                  (workshopOption ? Object.keys(workshopOption).length : 0) !==
                  requireOption
                }
                mode="contained"
                onPress={() => this.setState({tab: 3})}
                style={{marginBottom: 8}}>
                Lanjut
              </Button>
              <Button onPress={() => this.backHandle()}>Kembali</Button>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }

  renderSebagai() {
    const {
      as,
      keanggotaan,
      npa,
      bidang,
      spesialis,
      name,
      email,
      phone,
    } = this.state;
    let isvalid = false;
    if (keanggotaan === 'anggota') {
      isvalid = !!as && !!npa;
    }
    if (keanggotaan === 'non-anggota') {
      isvalid = !!as && !!name && !!email && !!phone && !!bidang;
    }
    return (
      <ScrollView style={styles.container} keyboardShouldPersistTaps={'always'}>
        <KeyboardAvoidingView style={styles.container}>
          <Card style={{marginBottom: 8}}>
            <Card.Content>
              <Title style={{fontSize: 18}}>Mendaftarkan sebagai?</Title>
              <RadioButton.Group
                onValueChange={(val) => this.setState({as: val})}
                value={as}>
                <RadioButton.Item label="Individual" value="self" />
                <RadioButton.Item label="Farmasi/Institusi" value="sponsor" />
              </RadioButton.Group>
            </Card.Content>
          </Card>
          <Card style={{marginBottom: 8}}>
            <Card.Content>
              <Title style={{fontSize: 18}}>
                Apakah peserta merupakan anggota PAPDI yang terdaftar?
              </Title>
              <RadioButton.Group
                onValueChange={(val) => this.setState({keanggotaan: val})}
                value={keanggotaan}>
                <RadioButton.Item label="Anggota PAPDI" value="anggota" />
                <RadioButton.Item
                  label="Bukan Anggota PAPDI"
                  value="non-anggota"
                />
              </RadioButton.Group>
            </Card.Content>
          </Card>
          {keanggotaan === 'anggota' && (
            <Card style={{marginBottom: 8}}>
              <Card.Content>
                <Title style={{marginBottom: 10, fontSize: 18}}>
                  No Pokok Anggota (NPA PAPDI){' '}
                </Title>
                <TextInput
                  label="NPA PAPDI"
                  value={npa}
                  mode={'outlined'}
                  onChangeText={(val) => this.setState({npa: val})}
                  keyboardType={'number-pad'}
                  render={(props) => (
                    <TextInputMask
                      {...props}
                      mask="[000].[0000].[0000].[00000]"
                    />
                  )}
                />
                <Paragraph style={{marginTop: 8}}>
                  Bagi anggota papdi diperlukan NPA PAPDI yang akan menentukan
                  harga paket, Hubungi kami untuk informasi nomor keanggotaan.
                </Paragraph>
              </Card.Content>
            </Card>
          )}
          {keanggotaan === 'non-anggota' && (
            <React.Fragment>
              <Card style={{marginBottom: 8}}>
                <Card.Content>
                  <Title style={{marginBottom: 10, fontSize: 18}}>
                    Lengkapi Data Peserta
                  </Title>
                  <TextInput
                    label="Nama Lengkap Peserta"
                    value={name}
                    mode={'outlined'}
                    style={{marginBottom: 6}}
                    onChangeText={(val) => this.setState({name: val})}
                  />
                  <TextInput
                    label="Alamat Email Peserta"
                    autoCapitalize={'none'}
                    value={email}
                    mode={'outlined'}
                    style={{marginBottom: 6}}
                    onChangeText={(val) => this.setState({email: val})}
                  />
                  <TextInput
                    label="Nomor Telepon Peserta"
                    value={phone}
                    mode={'outlined'}
                    style={{marginBottom: 6}}
                    onChangeText={(val) => this.setState({phone: val})}
                  />
                </Card.Content>
              </Card>

              <Card style={{marginBottom: 8}}>
                <Card.Content>
                  <Title style={{marginBottom: 10, fontSize: 18}}>
                    Bidang Peserta
                  </Title>
                  <RadioButton.Group
                    onValueChange={(val) => this.setState({bidang: val})}
                    value={bidang}>
                    <RadioButton.Item label="Dokter Umum" value="umum" />
                    <RadioButton.Item label="Spesialis" value="spesialis" />
                  </RadioButton.Group>
                </Card.Content>
              </Card>
              {bidang === 'spesialis' && (
                <Card style={{marginBottom: 8}}>
                  <Card.Content>
                    <Title style={{marginBottom: 10, fontSize: 18}}>
                      Bidang Spesialis
                    </Title>
                    <Picker
                      mode="dropdown"
                      placeholder="Pilih Bidang"
                      placeholderStyle={{color: '#bfc6ea'}}
                      placeholderIconColor="#007aff"
                      style={{width: undefined}}
                      selectedValue={spesialis}
                      onValueChange={(val) => this.setState({spesialis: val})}>
                      <Picker.Item
                        label="Jantung dan Pembuluh Darah"
                        value="Jantung dan Pembuluh Darah"
                      />
                      <Picker.Item label="Paru" value="Paru" />
                      <Picker.Item label="Saraf" value="Saraf" />
                      <Picker.Item label="Anestesi" value="Anestesi" />
                      <Picker.Item label="Lain-lain" value="Lain-lain" />
                    </Picker>
                  </Card.Content>
                </Card>
              )}
            </React.Fragment>
          )}
          <View style={{padding: 16}}>
            <Button
              disabled={!isvalid}
              mode="contained"
              onPress={() => this.processMembership()}>
              Lanjut
            </Button>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    );
  }

  selectPackagePrice(item) {
    const {membership} = this.state;
    let price = item.price;
    switch (membership) {
      case 'anggota':
        price = item.price_member;
        break;
      case 'finasim':
        price = item.price_finasim;
        break;
      case 'umum':
        price = item.price_dokter_umum;
        break;
    }
    return price;
  }

  renderPackageItem(item, index) {
    const {membership} = this.state;
    let price = this.selectPackagePrice(item);
    if (item.package_rules.indexOf(membership) > -1) {
      return (
        <Card key={index} style={{marginBottom: 8}}>
          <Card.Content style={{paddingBottom: 16}}>
            <Title style={{fontSize: 16}}>
              {item.group_name + ' - ' + item.title}
            </Title>
            <Paragraph>{item.description}</Paragraph>
          </Card.Content>
          <Divider />
          <Card.Actions style={{paddingHorizontal: 16}}>
            <Grid style={{justifyContent: 'space-between'}}>
              <Title style={{fontSize: 18}}>Rp{formatMoney(price)}</Title>
              <Button
                onPress={() => this.processPackage(item)}
                mode="contained">
                Pilih
              </Button>
            </Grid>
          </Card.Actions>
        </Card>
      );
    }
  }

  renderPackage() {
    const {as, keanggotaan, npa, name, event} = this.state;
    // console.log(event.activePackages);
    return (
      <ScrollView style={styles.container} keyboardShouldPersistTaps={'always'}>
        <KeyboardAvoidingView style={styles.container}>
          <Card style={{marginBottom: 8}}>
            <Card.Content>
              <Title style={{fontSize: 18}}>Pilih paket sebagai {name}</Title>
            </Card.Content>
          </Card>
          {event.activePackages.map((item, index) => {
            return this.renderPackageItem(item, index);
          })}
          <View style={{padding: 16}}>
            <Button onPress={() => this.backHandle()}>Kembali</Button>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    );
  }

  renderConfirm() {
    const {
      as,
      keanggotaan,
      npa,
      bidang,
      spesialis,
      name,
      email,
      phone,
      workshopOption,
    } = this.state;
    return (
      <ScrollView style={styles.container} keyboardShouldPersistTaps={'always'}>
        <Card style={{marginBottom: 8}}>
          <Card.Content>
            <Title style={{fontSize: 18}}>Konfirmasi Data</Title>
            <Grid>
              <Col style={{paddingVertical: 8}}>
                <Subheading>Nama</Subheading>
              </Col>
              <Col style={{paddingVertical: 8}}>
                <Subheading style={{textAlign: 'right'}}>{name}</Subheading>
              </Col>
            </Grid>
            <Divider />
            <Grid>
              <Col style={{paddingVertical: 8}}>
                <Subheading>Email</Subheading>
              </Col>
              <Col style={{paddingVertical: 8}}>
                <Subheading style={{textAlign: 'right'}}>{email}</Subheading>
              </Col>
            </Grid>
            <Divider />
            <Grid>
              <Col style={{paddingVertical: 8}}>
                <Subheading>Telepon</Subheading>
              </Col>
              <Col style={{paddingVertical: 8}}>
                <Subheading style={{textAlign: 'right'}}>{phone}</Subheading>
              </Col>
            </Grid>
            <Divider />
            <Grid>
              <Col style={{paddingVertical: 8}}>
                <Subheading>Keanggotaan</Subheading>
              </Col>
              <Col style={{paddingVertical: 8}}>
                <Subheading style={{textAlign: 'right'}}>
                  {keanggotaan}
                </Subheading>
              </Col>
            </Grid>
            <Divider />
            <Grid>
              <Col style={{paddingVertical: 8}}>
                <Subheading>Bidang</Subheading>
              </Col>
              <Col style={{paddingVertical: 8}}>
                <Subheading style={{textAlign: 'right'}}>{bidang}</Subheading>
              </Col>
            </Grid>
            <Divider />
            <Subheading style={{paddingVertical: 8}}>
              Pilihan Workshop
            </Subheading>
            {workshopOption &&
              Object.keys(workshopOption).map((key, index) => (
                <Grid key={index}>
                  <Col style={{width: 20}}>
                    <Paragraph>{index + 1}</Paragraph>
                  </Col>
                  <Col>
                    <Paragraph>{workshopOption[key].name}</Paragraph>
                  </Col>
                </Grid>
              ))}
          </Card.Content>
        </Card>
        <View style={{padding: 16}}>
          <Button mode="contained" onPress={() => this.addToCart()}>
            Masukkan Keranjang
          </Button>
        </View>
      </ScrollView>
    );
  }

  backHandle() {
    const {tab} = this.state;
    if (tab > 0) {
      this.setState({tab: tab - 1});
    } else {
      this.props.navigation.goBack();
    }
  }

  async addToCart() {
    // console.log(this.state);
    const {
      event,
      selectedPackage,
      as,
      keanggotaan,
      npa,
      bidang,
      name,
      email,
      phone,
      workshopOption,
    } = this.state;
    /*console.log(
      Object.keys(workshopOption).map((key) => workshopOption[key].id),
    );*/
    const reqParams = {
      id: Date.now(),
      event_id: event.id,
      package_id: selectedPackage.id,
      package_member: keanggotaan,
      price: this.selectPackagePrice(selectedPackage),
      workshops: JSON.stringify(
        Object.keys(workshopOption).map((key) => workshopOption[key].id),
      ),
      title: selectedPackage.title,
      as: as,
      name: name,
      email: email,
      phone: phone,
      npa: npa,
      bidang: bidang,
      useajax: 'true',
      task: 'addCart',
    };

    const request = await api.event('cart', reqParams);
    const result = getValue(request, 'data', {default: false});
    if (!!result && typeof result === 'object') {
      this.props.dispatch(EventActions.eventSuccess('cart', result));
      this.props.navigation.replace('Cart');
    }
    // console.log(result);
  }

  render() {
    const {tab} = this.state;
    const tabStyle = {
      activeTabStyle: {backgroundColor: '#a65381'},
      tabStyle: {backgroundColor: '#a65381'},
      textStyle: {color: '#fff', fontSize: 13},
      activeTextStyle: {color: '#fff', fontSize: 13},
      disabled: true,
    };
    return (
      <View style={{flex: 1}}>
        <MainHeader
          left={[{type: 'customBack', action: () => this.backHandle()}]}
          right={['cart']}
          title={'Registrasi Event'}
          cart={this.props.cart}
        />
        <Tabs
          style={{backgroundColor: '#a65381'}}
          tabBarUnderlineStyle={{backgroundColor: '#e3e3e3'}}
          tabBarBackgroundColor={'#a65381'}
          locked={true}
          initialPage={0}
          onChangeTab={(tab) => {
            this.setState({tab: tab.i});
          }}
          page={tab}>
          <Tab {...tabStyle} heading="Sebagai">
            {this.renderSebagai()}
          </Tab>
          <Tab {...tabStyle} heading="Paket">
            {this.renderPackage()}
          </Tab>
          <Tab {...tabStyle} heading="Workshop">
            {this.renderWorkshops()}
          </Tab>
          <Tab {...tabStyle} heading="Konfirmasi">
            {this.renderConfirm()}
          </Tab>
        </Tabs>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cart: state.event.cart,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {dispatch};
};

export default connect(mapStateToProps, mapDispatchToProps)(EventRegister);
