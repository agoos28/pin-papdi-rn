import React, {Component} from 'react';
import {KeyboardAvoidingView, ScrollView, View} from 'react-native';
import {connect} from 'react-redux';
import styles from './Event.style';
import MainHeader from '../../Components/MainHeader';
import {getValue} from '../../Helper/Validator';
import API from '../../Services/Api';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import EmptyContent from '../../Components/EmptyContent';
import {Metrics} from '../../Themes';
import {Image} from 'react-native-animatable';
import {BottomNavigation, Text} from 'react-native-paper';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import EventDetailPengantarScreen from './EventDetailPengantarScreen';
import {Dimensions} from 'react-native';

const api = API.create();

const FirstRoute = () => (
  <View style={[styles.container, {backgroundColor: '#ff4081'}]} />
);

const SecondRoute = () => (
  <View style={[styles.container, {backgroundColor: '#673ab7'}]} />
);

class EventTabDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        {key: 'pengantar', title: 'Pengantar'},
        {key: 'paket', title: 'Paket'},
        {key: 'agenda', title: 'Agenda'},
        {key: 'workshop', title: 'Workshop'},
        {key: 'ketentuan', title: 'Ketentuan'},
      ],
    };
  }

  renderScene = ({route, jumpTo}) => {
    switch (route.key) {
      case 'pengantar':
        return <EventDetailPengantarScreen {...this.props} />;
      case 'paket':
        return <EventDetailPengantarScreen {...this.props} />;
      case 'agenda':
        return <EventDetailPengantarScreen {...this.props} />;
      case 'workshop':
        return <EventDetailPengantarScreen {...this.props} />;
      case 'ketentuan':
        return <EventDetailPengantarScreen {...this.props} />;
    }
  };

  renderTabBar = (props) => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={{backgroundColor: 'white'}}
      tabStyle={{width: 'auto'}}
    />
  );

  render() {
    const {index, routes} = this.state;
    return (
      <View style={{flex: 1}}>
        <TabView
          ovescroll
          scrollEnabled
          navigationState={{index, routes}}
          onIndexChange={(i) => this.setState({index: i})}
          renderScene={this.renderScene}
          labelStyle={{width: 200}}
          initialLayout={{width: Dimensions.get('window').width}}
          renderTabBar={this.renderTabBar}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(EventTabDetail);
