import React from 'react';
import {ScrollView, Text, KeyboardAvoidingView, View} from 'react-native';

import styles from './Event.style';
import {
  Card,
  DataTable,
  Divider,
  Paragraph,
  Subheading,
  Title,
} from 'react-native-paper';
import HTML from 'react-native-render-html';
import {formatDate, formatMoney} from '../../Helper/Formater';

export default class EventPackageItem extends React.PureComponent {
  renderPackageItem(workshop) {
    const res = [];
    workshop.map((item, index) => {
      res.push(
        <Card key={index} style={{marginBottom: 10}}>
          <Card.Title title={item.title} subtitle={item.description} />
          <Divider />
          <DataTable>
            {item.price_member !== '0' && (
              <DataTable.Row>
                <DataTable.Cell>Anggota PAPDI</DataTable.Cell>
                <DataTable.Cell>
                  Rp.{formatMoney(item.price_member)}
                </DataTable.Cell>
              </DataTable.Row>
            )}
            {item.price_finasim !== '0' && (
              <DataTable.Row>
                <DataTable.Cell>Anggota PAPDI Finasim</DataTable.Cell>
                <DataTable.Cell>
                  Rp.{formatMoney(item.price_finasim)}
                </DataTable.Cell>
              </DataTable.Row>
            )}
            {item.price !== '0' && (
              <DataTable.Row>
                <DataTable.Cell>Spesialis Lainnya</DataTable.Cell>
                <DataTable.Cell>Rp.{formatMoney(item.price)}</DataTable.Cell>
              </DataTable.Row>
            )}
            {item.price_dokter_umum !== '0' && (
              <DataTable.Row>
                <DataTable.Cell>Dokter Umum</DataTable.Cell>
                <DataTable.Cell>
                  Rp.{formatMoney(item.price_dokter_umum)}
                </DataTable.Cell>
              </DataTable.Row>
            )}
          </DataTable>
        </Card>,
      );
    });
    return res;
  }
  render() {
    const {workshop} = this.props;
    return (
      <ScrollView style={styles.container}>
        <View style={{paddingHorizontal: 16, paddingVertical: 10}}>
          <Title>{workshop.title}</Title>
          <Paragraph>
            {'Berlaku ' +
              formatDate(workshop.start_date) +
              ' s/d ' +
              formatDate(workshop.end_date)}
          </Paragraph>
        </View>
        {this.renderPackageItem(workshop.items)}
        <Card.Content style={{paddingLeft: 0, paddingTop: 20}}>
          <HTML tagsStyles={{p: {margin: 0}}} html={workshop.description} />
        </Card.Content>
      </ScrollView>
    );
  }
}
