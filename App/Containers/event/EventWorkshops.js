import React, {Component} from 'react';
import {ScrollView, View} from 'react-native';
import {connect} from 'react-redux';
import styles from './Event.style';
import MainHeader from '../../Components/MainHeader';
import {Card, List, Paragraph} from 'react-native-paper';
class EventWorkshops extends Component {
  constructor(props) {
    super(props);
    const navigationParams = props.navigation.getParam('params', null);
    this.state = {
      expanded: null,
      workshops: navigationParams,
    };
  }

  renderWorkshop(activities) {
    const el = [
      <View
        key={'head'}
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          paddingHorizontal: 16,
          borderBottomWidth: 0.5,
          paddingVertical: 5,
          borderColor: '#cfcfcfb3',
          backgroundColor: '#f7f7f7',
        }}>
        <View style={{flexGrow: 0, width: 65}}>
          <Paragraph>Kode</Paragraph>
        </View>
        <View style={{flex: 1, flexGrow: 1}}>
          <Paragraph>Judul</Paragraph>
        </View>
        <View style={{flexGrow: 0, width: 34}}>
          <Paragraph style={{textAlign: 'center'}}>Kursi</Paragraph>
        </View>
      </View>,
    ];
    activities.map((item, index) => {
      el.push(
        <View
          key={index}
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-start',
            paddingHorizontal: 16,
            borderBottomWidth: 0.5,
            paddingVertical: 5,
            borderColor: '#cfcfcfb3',
            backgroundColor: index % 2 ? '#f7f7f7' : '#fffff',
          }}>
          <View style={{flexGrow: 0, width: 65}}>
            <Paragraph>{item.code}</Paragraph>
          </View>
          <View style={{flex: 1, flexGrow: 1, paddingRight: 10}}>
            <Paragraph>{item.name}</Paragraph>
          </View>
          <View style={{flexGrow: 0, width: 34}}>
            <Paragraph style={{textAlign: 'center'}}>{item.seats}</Paragraph>
          </View>
        </View>,
      );
    });
    el.push(
      <View key={'footer'} style={{height: 5, backgroundColor: '#cfcfcfb3'}} />,
    );
    return el;
  }

  renderWorkshops() {
    const {workshops, expanded} = this.state;
    const el = [];
    if (!workshops) {
      return null;
    }
    workshops.map((item, index) => {
      el.push(
        <List.Accordion
          key={index}
          title={!!item.name && item.name.replace('&amp;', '&')}
          id={item.id}
          expanded={index === expanded}
          onPress={() =>
            this.setState({expanded: index === expanded ? undefined : index})
          }
          style={{
            borderBottomWidth: 0.5,
            borderColor: '#cfcfcfb3',
            backgroundColor: index === expanded ? '#a65381' : '#fff',
          }}
          titleStyle={{
            color: index === expanded ? '#fff' : '#333',
          }}>
          {this.renderWorkshop(item.activities)}
        </List.Accordion>,
      );
    });
    return el;
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <MainHeader left={['back']} title={'Workshop'} />
        <ScrollView style={styles.container}>
          <Card>{this.renderWorkshops()}</Card>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(EventWorkshops);
