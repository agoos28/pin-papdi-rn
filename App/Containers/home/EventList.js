import React, {Component} from 'react';
import {FlatList, RefreshControl, View, Text} from 'react-native';
import {connect} from 'react-redux';
import API from '../../Services/Api';
import styles from './EventsListStyle';
import {getValue} from '../../Helper/Validator';
import EmptyContent from '../../Components/EmptyContent';
import {Metrics} from '../../Themes';
import {ActivityIndicator, Button, Card, Paragraph} from 'react-native-paper';
import MainHeader from '../../Components/MainHeader';
import AuthActions from '../../Redux/AuthRedux';

const api = API.create();

class EventList extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      tabBarVisible: !navigation.getParam('guest', false),
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      loading: false,
      refreshing: false,
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.auth.guest !== prevProps.auth.guest) {
      this.props.navigation.setParams({guest: this.props.auth.guest});
    }
  }

  componentDidMount() {
    this.props.navigation.setParams({guest: this.props.auth.guest});
    this.getData();
  }

  checkLogin(request) {
    const {auth, login} = this.props;
    const guest = getValue(request, 'data.guest', {default: false});
    if (guest === true && auth.guest === false) {
      login({
        username: auth.user.email,
        passwd: auth.password,
        task: 'login',
      });
    }
  }

  async getData() {
    this.setState({
      loading: true,
      refreshing: !!this.state.items.length,
    });
    const reqParams = {};
    const request = await api.event('getList', reqParams);
    this.checkLogin(request);
    const result = getValue(request, 'data.data', {default: []});
    this.setState({
      items: result,
      loading: false,
      refreshing: false,
    });
  }

  // Render a header?
  renderHeader = () => (
    <Text style={[styles.label, styles.sectionHeader]}> - Header - </Text>
  );

  // Render a footer?
  renderFooter = () => (
    <Text style={[styles.label, styles.sectionHeader]}> - Footer - </Text>
  );

  // Show this when data is empty
  renderEmpty = () => {
    const {loading} = this.state;
    if (loading) {
      return <ActivityIndicator animating />;
    }
    return (
      <EmptyContent
        textDescription={'Event kosong atau gangguan pada jaringan.'}
      />
    );
  };

  keyExtractor = (item, index) => item.id;

  // How many items should be kept im memory as we scroll?
  oneScreensWorth = 20;

  renderItem({item}) {
    if (!item) {
    }
    return (
      <Card key={item.id}>
        <Card.Cover
          source={{uri: item.image}}
          style={{
            width: Metrics.screenWidth - 30,
            height: Metrics.screenWidth * (1280 / 902) * 0.7,
          }}
        />
        <Card.Title title={item.title} />
        <Card.Content>
          <Paragraph>
            {item.description.substring(0, item.description.indexOf('.'))}
          </Paragraph>
        </Card.Content>
        <Card.Actions>
          <Button
            onPress={() =>
              this.props.navigation.navigate('EventDetail', {
                params: item,
              })
            }>
            Selengkapnya
          </Button>
        </Card.Actions>
      </Card>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <MainHeader
          title={'PAPDI'}
          right={['profile', 'cart']}
          cart={this.props.cart}
        />
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this.getData()}
            />
          }
          contentContainerStyle={styles.listContent}
          data={this.state.items}
          renderItem={this.renderItem.bind(this)}
          numColumns={1}
          keyExtractor={this.keyExtractor}
          initialNumToRender={10}
          ListEmptyComponent={this.renderEmpty}
        />
        {this.props.auth.guest && (
          <Button
            mode={'contained'}
            style={{borderRadius: 0}}
            onPress={() => this.props.navigation.push('Login')}>
            Login
          </Button>
        )}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    cart: state.event.cart,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    login: (params) => dispatch(AuthActions.authRequest('login', params)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EventList);
