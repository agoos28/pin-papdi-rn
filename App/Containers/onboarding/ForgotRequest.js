import React from 'react';
import {
  View,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  Alert,
  TouchableOpacity,
  Text,
  Platform,
} from 'react-native';
import {connect} from 'react-redux';
import Styles from './LoginStyle';
import API from '../../Services/Api';
import {getValue} from '../../Helper/Validator';
import Button from '../../Components/Button';
import {BaseStyle} from '../../Themes';
import Images from '../../Themes/Images';
import {Appbar, Paragraph, Title} from 'react-native-paper';
import TextInput from '../../Components/TextInput';
import MainHeader from '../../Components/MainHeader';
const api = API.create();

class ForgotRequest extends React.PureComponent {
  static navigationOptions = {
    headerStyle: {
      marginTop: 24,
    },
  };

  constructor(props) {
    super(props);
    this.state = {
      email: null,
      loading: false,
      errors: {
        email: '',
      },
    };
  }

  async handleSubmit() {
    let error = false;

    switch (true) {
      case !this.state.email:
        error = 'Email required';
        break;
      default:
        error = false;
    }

    if (error) {
      Alert.alert('Check form', error, [{text: 'OK', onPress: () => false}]);
      return false;
    }
    const reqParams = {
      email: this.state.email,
    };
    this.setState({loading: true});
    const request = await api.user('forgotPassword', reqParams);
    this.setState({loading: false});
    const result = getValue(request, 'data', {default: null});
    if (!request.ok) {
      Alert.alert('Error', result.message);
    } else {
      this.props.navigation.navigate('ForgotToken', {email: this.state.email});
    }
  }

  render() {
    const {loading, email, errors} = this.state;
    return (
      <View style={{flex: 1}}>
        <MainHeader title={'Lupa Password'} left={['back']} />
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
          style={{flex: 1}}>
          <ScrollView
            style={BaseStyle.container}
            keyboardShouldPersistTaps={'handled'}>
            <View style={[Styles.container]}>
              <Image source={Images.logo} style={Styles.logo} />
              <Paragraph style={{marginBottom: 20}}>
                Silakan masukkan alamat email untuk akun anda. Token verifikasi
                akan dikirim ke email anda. Setelah token diterima, anda dapat
                memilih sandi lewat baru untuk akun anda.
              </Paragraph>
              <TextInput
                label={'Email'}
                value={email}
                onChangeText={(text) =>
                  this.setState({
                    email: text,
                    errors: {
                      ...errors,
                      email: '',
                    },
                  })
                }
                error={!!errors.email}
                errorText={errors.email}
                autoCapitalize="none"
                autoCompleteType="email"
                textContentType="emailAddress"
                keyboardType="email-address"
              />
              <Button
                onPress={() => this.handleSubmit()}
                loading={loading}
                disabled={loading}>
                <Text>Lanjut</Text>
              </Button>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {auth, app} = state;
  return {auth, app};
};

const mapDispatchToProps = (dispatch) => {
  return {dispatch};
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgotRequest);
