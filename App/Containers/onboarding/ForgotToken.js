import React from 'react';
import {
  View,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  Alert,
  TouchableOpacity,
  Text,
  Platform,
} from 'react-native';
import {connect} from 'react-redux';
import Styles from './LoginStyle';
import API from '../../Services/Api';
import {getValue} from '../../Helper/Validator';
import Button from '../../Components/Button';
import {BaseStyle} from '../../Themes';
import Images from '../../Themes/Images';
import {Appbar, Paragraph, Title} from 'react-native-paper';
import TextInput from '../../Components/TextInput';
import MainHeader from '../../Components/MainHeader';
const api = API.create();

class ForgotToken extends React.PureComponent {
  static navigationOptions = {
    headerStyle: {
      marginTop: 24,
    },
  };

  constructor(props) {
    super(props);
    const {params} = props.navigation.state;
    this.state = {
      email: params.email,
      token: '',
      loading: false,
      errors: {
        token: '',
      },
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.fetching === true && nextProps.auth.fetching === false) {
      return {
        showAlert: true,
        fetching: nextProps.auth.fetching,
      };
    } else {
      return {
        showAlert: false,
        fetching: nextProps.auth.fetching,
      };
    }
  }

  async handleSubmit() {
    let error = false;

    switch (true) {
      case !this.state.token:
        error = 'Token diperlukan';
        break;
      default:
        error = false;
    }

    if (error) {
      Alert.alert('Check form', error, [{text: 'OK', onPress: () => false}]);
      return false;
    }
    const reqParams = {
      token: this.state.token,
      email: this.state.email,
    };
    this.setState({loading: true});
    const request = await api.user('confirmReset', reqParams);
    this.setState({loading: false});
    const result = getValue(request, 'data', {default: null});
    if (!request.ok) {
      Alert.alert('Error', result.message);
      return;
    } else {
      this.props.navigation.navigate('ResetPassword');
    }
  }

  render() {
    const {token, errors} = this.state;
    return (
      <View style={{flex: 1}}>
        <MainHeader title={'Buat Akun'} left={['back']} />
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
          style={{flex: 1}}>
          <ScrollView
            style={BaseStyle.container}
            keyboardShouldPersistTaps={'handled'}>
            <View style={Styles.container}>
              <Image source={Images.logo} style={Styles.logo} />
              <Title style={{marginBottom: 15}}>Lupa Password</Title>
              <Paragraph style={{marginBottom: 20}}>
                Silakan masukkan token yang dikirim ke alamat email anda.
              </Paragraph>
              <TextInput
                label={'Token'}
                value={token}
                autoFocus={true}
                onChangeText={(text) =>
                  this.setState({
                    token: text,
                    errors: {
                      ...errors,
                      token: '',
                    },
                  })
                }
                error={!!errors.token}
                errorText={errors.token}
                autoCapitalize="none"
              />
              <Button
                onPress={() => this.handleSubmit()}
                loading={this.state.loading}
                disabled={this.state.loading}>
                <Text>Lanjut</Text>
              </Button>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {auth, app} = state;
  return {auth, app};
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgotToken);
