import React, {Component} from 'react';
import {
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
  Alert,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
} from 'react-native';
import {connect} from 'react-redux';
import AuthActions from '../../Redux/AuthRedux';
import Images from '../../Themes/Images';
import Styles from './LoginStyle';
import {getValue, validateEmail} from '../../Helper/Validator';
import {BaseStyle, Colors} from '../../Themes';
import BackButton from '../../Components/BackButton';
import {Appbar, Paragraph, Title} from 'react-native-paper';
import TextInput from '../../Components/TextInput';
import Button from '../../Components/Button';
import EventActions from '../../Redux/EventRedux';
import API from '../../Services/Api';
const api = API.create();

class Login extends Component {
  constructor(props) {
    super(props);
    const {params} = props.navigation.state;
    this.state = {
      message: params?.message,
      isReady: false,
      method: 'email',
      email: '',
      password: '',
      phone: null,
      firebasePermissionRequest: true,
      loading: false,
      errors: {
        email: '',
        password: '',
      },
    };
  }

  async getCart() {
    const reqParams = {
      task: 'getCart',
      format: 'json',
    };
    const request = await api.event('cart', reqParams);
    const result = getValue(request, 'data', {default: null});

    if (typeof result !== 'object') {
      return;
    }

    this.props.dispatch(EventActions.eventSuccess('cart', result));
    this.setState({cart: result});

    return;
  }

  async UNSAFE_componentWillMount() {
    const {guest} = this.props;
    if (!guest) {
      await this.getCart();
      this.props.navigation.replace('Home');
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    const {guest} = this.props;
    if (!guest) {
      await this.getCart();
      this.props.navigation.popToTop();
    }
  }

  async login() {
    let errorMessage = false;

    if (!this.state.password) {
      errorMessage = 'Password belum terisi!';
    }
    if (!validateEmail(this.state.email)) {
      // errorMessage = 'Periksa kembali email!';
    }
    if (!this.state.email) {
      errorMessage = 'Email belum terisi!';
    }
    if (errorMessage !== false) {
      Alert.alert(errorMessage);
      return;
    }

    Keyboard.dismiss();

    const reqParams = {
      username: this.state.email,
      passwd: this.state.password,
    };

    this.setState({loading: true});
    const request = await api.user('login', reqParams);
    this.setState({loading: false});
    const result = getValue(request, 'data', {default: null});
    if (!request.ok) {
      Alert.alert('Login Gagal', result.message);
    } else {
      this.props.saveLogin(result.data);
      // this.props.navigation.navigate('ForgotToken', {email: this.state.email});
    }
  }

  render() {
    const {email, password, errors, message} = this.state;
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={{flex: 1}}>
        <ScrollView
          style={BaseStyle.container}
          keyboardShouldPersistTaps={'handled'}>
          <View style={[Styles.container, {paddingTop: 100}]}>
            <Image source={Images.logo} style={Styles.logo} />
            <Title>Login</Title>
            <Paragraph style={{marginBottom: 15}}>{message}</Paragraph>
            <TextInput
              label={'Email'}
              returnKeyType={'next'}
              value={email}
              onChangeText={(text) =>
                this.setState({
                  email: text,
                  errors: {
                    ...errors,
                    email: '',
                  },
                })
              }
              error={!!errors.email}
              errorText={errors.email}
              autoCapitalize="none"
              autoCompleteType="email"
              textContentType="emailAddress"
              keyboardType="email-address"
            />

            <TextInput
              label={'Password'}
              returnKeyType={'done'}
              value={password}
              onChangeText={(text) =>
                this.setState({
                  password: text,
                  errors: {
                    ...errors,
                    password: '',
                  },
                })
              }
              error={!!errors.password}
              errorText={errors.password}
              secureTextEntry
            />

            <View style={Styles.forgotPassword}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('ForgotRequest')}>
                <Text style={Styles.label}>Lupa Sandi Lewat?</Text>
              </TouchableOpacity>
            </View>

            <Button
              onPress={() => this.login()}
              loading={this.state.loading}
              disabled={this.state.loading}>
              <Text>Sign In</Text>
            </Button>

            <View style={Styles.row}>
              <Text style={Styles.label}>Buat akun, </Text>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Register')}>
                <Text style={Styles.link}>Daftar</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = (state) => {
  const {auth} = state;
  return {
    loading: auth.fetching,
    guest: auth.guest,
    user: auth.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
    saveLogin: (params) => dispatch(AuthActions.authSuccess('login', params)),
    logout: (params) => dispatch(AuthActions.authRequest('logout', params)),
    getProfile: (userId) =>
      dispatch(AuthActions.authRequest('getProfile', userId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
