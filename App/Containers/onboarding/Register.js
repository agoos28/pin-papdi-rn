import React from 'react';
import {
  Alert,
  Image,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import styles from './RegisterStyle';
import {Appbar, Checkbox, Title} from 'react-native-paper';
import {getValue, validateEmail} from '../../Helper/Validator';
import {BaseStyle} from '../../Themes';
import Button from '../../Components/Button';
import MainHeader from '../../Components/MainHeader';
import TextInput from '../../Components/TextInput';
import AuthActions from '../../Redux/AuthRedux';
import Images from '../../Themes/Images';
import API from '../../Services/Api';

const api = API.create();

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      name: __DEV__ ? 'Agus' : '',
      email: __DEV__ ? 'agus_riyanto_28@yahoo.com' : '',
      password: __DEV__ ? 'admin@123' : '',
      password2: __DEV__ ? 'admin@123' : '',
      phone: __DEV__ ? '087899882346' : '',
      firebasePermissionRequest: true,
      loading: false,
      errors: {
        name: '',
        email: '',
        password: '',
      },
    };
  }

  async register() {
    let error = '';
    const {name, email, phone, password, password2} = this.state;
    switch (true) {
      case !name:
        error = 'Nama lengkap diperlukan';
        break;
      case !email:
        error = 'Alamat email diperlukan';
        break;
      case !phone:
        error = 'Nomor telpon diperlukan';
        break;
      case !password:
        error = 'Password diperlukan';
        break;
      case !password2:
        error = 'Konfirmasi password diperlukan';
        break;
      case password2 !== password:
        error = '';
        break;
      default:
        error = false;
    }

    if (error) {
      Alert.alert('Check form', error, [{text: 'OK', onPress: () => false}]);
      return false;
    }
    const reqParams = {name, email, phone, password, password2};
    this.setState({loading: true});
    const request = await api.user('register', reqParams);
    this.setState({loading: false});
    const result = getValue(request, 'data', {default: null});
    if (!request.ok) {
      Alert.alert('Error', result.message);
    } else {
      Alert.alert('Sukses', result.message);
      this.props.navigation.navigate('Login');
    }
  }

  render() {
    const {name, email, password, password2, phone, errors} = this.state;
    return (
      <View style={{flex: 1}}>
        <MainHeader title={'Buat Akun'} left={['back']} />
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
          style={{flex: 1}}>
          <ScrollView
            style={BaseStyle.container}
            keyboardShouldPersistTaps={'handled'}>
            <View style={styles.container}>
              <Image source={Images.logo} style={styles.logo} />
              <TextInput
                label={'Nama Lengkap'}
                returnKeyType={'next'}
                value={name}
                autoFocus={true}
                onChangeText={(text) =>
                  this.setState({
                    name: text,
                    errors: {
                      ...errors,
                      name: '',
                    },
                  })
                }
              />

              <TextInput
                label={'Alamat Email'}
                returnKeyType={'next'}
                value={email}
                onChangeText={(text) =>
                  this.setState({
                    email: text,
                    errors: {
                      ...errors,
                      email: '',
                    },
                  })
                }
                autoCapitalize="none"
                autoCompleteType="email"
                textContentType="emailAddress"
                keyboardType="email-address"
              />

              <TextInput
                label={'Nomor Telpon'}
                returnKeyType={'next'}
                value={phone}
                onChangeText={(text) =>
                  this.setState({
                    phone: text,
                    errors: {
                      ...errors,
                      name: '',
                    },
                  })
                }
              />

              <TextInput
                label={'Password'}
                returnKeyType={'done'}
                value={password}
                onChangeText={(text) =>
                  this.setState({
                    password: text,
                    errors: {
                      ...errors,
                      password: '',
                    },
                  })
                }
                error={!!errors.password}
                errorText={errors.password}
                secureTextEntry
              />

              <TextInput
                label={'Konfirmasi Password'}
                returnKeyType={'done'}
                value={password2}
                onChangeText={(text) =>
                  this.setState({
                    password2: text,
                    errors: {
                      ...errors,
                      password: '',
                    },
                  })
                }
                error={!!errors.password}
                errorText={errors.password}
                secureTextEntry
              />

              <Button
                onPress={() => this.register()}
                loading={this.state.loading}
                disabled={this.state.loading}>
                <Text>Buat Akun</Text>
              </Button>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginBottom: 30,
                }}>
                <Checkbox
                  color={'#ccc'}
                  status={'checked'}
                  onPress={() => null}
                />
                <Text>{'Menerima Syarat dan Ketentuan'}</Text>
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    register: (params) => dispatch(AuthActions.authRequest('register', params)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
