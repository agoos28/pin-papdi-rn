import React from 'react';
import {ScrollView, Text} from 'react-native';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './ResetStyle';

const Reset = () => {
  return (
    <ScrollView style={styles.container}>
      <Text>Reset Container</Text>
    </ScrollView>
  );
};

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Reset);
