import React from 'react';
import {
  View,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  Alert,
  TouchableOpacity,
  Text,
  Platform,
} from 'react-native';
import {connect} from 'react-redux';
import Styles from './LoginStyle';
import API from '../../Services/Api';
import {getValue} from '../../Helper/Validator';
import Button from '../../Components/Button';
import {BaseStyle} from '../../Themes';
import Images from '../../Themes/Images';
import {Appbar, Paragraph, Title} from 'react-native-paper';
import TextInput from '../../Components/TextInput';
import * as NavigationActions from '@react-navigation/core/src/NavigationActions';
import MainHeader from '../../Components/MainHeader';
const api = API.create();

class ResetPassword extends React.PureComponent {
  static navigationOptions = {
    headerStyle: {
      marginTop: 24,
    },
  };

  constructor(props) {
    super(props);
    this.state = {
      password1: '',
      password2: '',
      loading: false,
      errors: {
        token: '',
      },
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.fetching === true && nextProps.auth.fetching === false) {
      return {
        showAlert: true,
        fetching: nextProps.auth.fetching,
      };
    } else {
      return {
        showAlert: false,
        fetching: nextProps.auth.fetching,
      };
    }
  }

  async handleSubmit() {
    const {password1, password2} = this.state;
    let error = false;

    switch (true) {
      case !password1:
        error = 'Password baru diperlukan';
        break;
      case !password2:
        error = 'Konfirmasi password baru diperlukan';
        break;
      case password2 !== password1:
        error = '';
        break;
      default:
        error = false;
    }

    if (error) {
      Alert.alert('Check form', error, [{text: 'OK', onPress: () => false}]);
      return false;
    }
    const reqParams = {
      password1: password1,
      password2: password2,
    };
    this.setState({loading: true});
    const request = await api.user('completereset', reqParams);
    this.setState({loading: false});
    const result = getValue(request, 'data', {default: null});
    if (!request.ok) {
      Alert.alert('Error', result.message);
      return;
    } else {
      Alert.alert('Sukses', result.message);
      this.props.navigation.reset(
        [
          NavigationActions.navigate({
            routeName: 'Login',
            params: {
              message: 'Silahkan Login dengan password baru anda.',
            },
          }),
        ],
        0,
      );
    }
  }

  render() {
    const {password1, password2, errors} = this.state;
    return (
      <View style={{flex: 1}}>
        <Appbar.Header>
          <Appbar.BackAction onPress={() => this.props.navigation.goBack()} />
        </Appbar.Header>
        <MainHeader title={'Reset Password'} left={['back']} />
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
          style={{flex: 1}}>
          <ScrollView
            style={BaseStyle.container}
            keyboardShouldPersistTaps={'always'}>
            <View style={Styles.container}>
              <Image source={Images.logo} style={Styles.logo} />
              <Paragraph style={{marginBottom: 20}}>
                Silakan buat baru password untuk akun anda.
              </Paragraph>
              <TextInput
                label={'Password Baru'}
                returnKeyType={'done'}
                value={password1}
                onChangeText={(text) =>
                  this.setState({
                    password1: text,
                    errors: {
                      ...errors,
                      password1: '',
                    },
                  })
                }
                error={!!errors.password1}
                errorText={errors.password1}
                secureTextEntry
              />
              <TextInput
                label={'Konfirmasi Password Baru'}
                returnKeyType={'done'}
                value={password2}
                onChangeText={(text) =>
                  this.setState({
                    password2: text,
                    errors: {
                      ...errors,
                      password2: '',
                    },
                  })
                }
                error={!!errors.password2}
                errorText={errors.password2}
                secureTextEntry
              />
              <Button
                onPress={() => this.handleSubmit()}
                loading={this.state.loading}
                disabled={this.state.loading}>
                <Text>Lanjut</Text>
              </Button>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {auth, app} = state;
  return {auth, app};
};

const mapDispatchToProps = (dispatch) => {
  return {dispatch};
};

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);
