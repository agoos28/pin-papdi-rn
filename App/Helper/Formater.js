import moment from 'moment';

export const MoneyFormat = (
  amount,
  decimalCount = 0,
  decimal = '',
  thousands = '.',
  useSymbol = true,
) => {
  try {
    let mathDecimalCount = Math.abs(decimalCount);
    let dc = isNaN(mathDecimalCount) ? 2 : mathDecimalCount;
    const negativeSign = amount < 0 ? '-' : '';
    let i = parseInt(Math.abs(Number(amount) || 0).toFixed(dc)).toString();
    let j = i.length > 3 ? i.length % 3 : 0;
    const formated =
      negativeSign +
      (j ? i.substr(0, j) + thousands : '') +
      i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousands) +
      (dc
        ? decimal +
          Math.abs(amount - i)
            .toFixed(dc)
            .slice(2)
        : '');
    return useSymbol ? 'Rp. ' + formated : formated;
  } catch (e) {
    console.log(e);
  }
};

export const MoneyFormatSimple = (amount) => {
  const result = MoneyFormat(Math.ceil(amount / 1000), 0, '', '.', false);
  return 'Rp ' + result + 'rb';
};

export const Capitalize = (string) => {
  return string[0].toUpperCase() + string.slice(1);
};

export const Pad = (n, width) => {
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
};

export const formatDate = (dateStr, format = 'DD MMM YYYY') => {
  moment.locale('id');
  const d = moment(dateStr);
  return d.locale('en').format(format);
};

export const formatTime = (dateStr, format = 'HH:mm') => {
  return formatDate(dateStr, format);
};

export const formatDateTime = (dateStr) => {
  return formatDate(dateStr) + ' ' + formatTime(dateStr);
};

export const readableTime = (timestamp) => {
  const d = moment.duration(timestamp);
  return {
    milliseconds: d.milliseconds(),
    seconds: d.seconds(),
    minutes: d.minutes(),
    hours: d.hours(),
    timestamp: timestamp,
    formated:
      Pad(d.hours(), 2) +
      ':' +
      Pad(d.minutes(), 2) +
      ':' +
      Pad(d.seconds(), 2) +
      '.' +
      Pad(d.milliseconds(), 3),
  };
};

export const groupArray = (xs, key) => {
  return xs.reduce(function (rv, x) {
    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;
  }, {});
};

export const formatMoney = (
  amount,
  decimalCount = 0,
  decimal = ',',
  thousands = '.',
) => {
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

    const negativeSign = amount < 0 ? '-' : '';

    let i = parseInt(
      (amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)),
    ).toString();
    let j = i.length > 3 ? i.length % 3 : 0;

    return (
      negativeSign +
      (j ? i.substr(0, j) + thousands : '') +
      i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousands) +
      (decimalCount
        ? decimal +
          Math.abs(amount - i)
            .toFixed(decimalCount)
            .slice(2)
        : '')
    );
  } catch (e) {
    console.log(e);
  }
};
