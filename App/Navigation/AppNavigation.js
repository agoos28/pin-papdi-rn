import React from 'react';
import {createAppContainer} from 'react-navigation';
import Cart from '../Containers/cart/Cart';
import CartDetail from '../Containers/cart/CartDetail';
import CartCheckout from '../Containers/cart/CartCheckout';
import SnapPayment from '../Containers/cart/SnapPayment';
import EventDetail from '../Containers/event/EventDetail';
import EventPackages from '../Containers/event/EventPackages';
import EventWorkshops from '../Containers/event/EventWorkshops';
import EventAgenda from '../Containers/event/EventAgenda';
import EventList from '../Containers/home/EventList';
import EventContent from '../Containers/event/EventContent';
import EventRegister from '../Containers/event/EventRegister';
import Account from '../Containers/account/Account';
import Profile from '../Containers/account/Profile';
import Transaction from '../Containers/account/Transaction';
import TransactionDetail from '../Containers/account/TransactionDetail';
import MyEvent from '../Containers/account/MyEvent';
import MyEventDetail from '../Containers/account/MyEventDetail';
import Reset from '../Containers/onboarding/Reset';
import Register from '../Containers/onboarding/Register';
import Login from '../Containers/onboarding/Login';
import OnBoarding from '../Containers/onboarding';
import {createStackNavigator} from 'react-navigation-stack';
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';
import LaunchScreen from '../Containers/LaunchScreen';

import styles from './Styles/NavigationStyles';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Color from '../Themes/PaperTheme';
import ForgotRequest from '../Containers/onboarding/ForgotRequest';
import ForgotToken from '../Containers/onboarding/ForgotToken';
import ResetPassword from '../Containers/onboarding/ResetPassword';
import { Colors } from '../Themes'

const HomeNav = createMaterialBottomTabNavigator(
  {
    Event: {screen: EventList, navigationOptions: {title: 'Home'}},
    MyEvent: {screen: MyEvent, navigationOptions: {title: 'My Event'}},
    Transaction: {
      screen: Transaction,
      navigationOptions: {title: 'My Transaction'},
    },
  },
  {
    headerMode: 'none',
    initialRouteName: 'Event',
    backBehavior: 'initialRoute',
    swipeEnabled: true,
    activeColor: 'white',
    inactiveColor: 'rgba(255,255,255,0.4)',
    labeled: true,
    shifting: false,
    barStyle: {backgroundColor: Colors.primary},
    defaultNavigationOptions: ({navigation}) => {
      return {
        tabBarIcon: ({focused, horizontal, tintColor}) => {
          const {routeName} = navigation.state;
          let iconName;
          switch (routeName) {
            case 'Event':
              iconName = 'home-outline';
              break;
            case 'MyEvent':
              iconName = 'calendar-check';
              break;
            case 'Transaction':
              iconName = 'receipt';
              break;
          }
          return <Icon name={iconName} size={25} color={tintColor} />;
        },
      };
    },
  },
);

// Manifest of possible screens
const PrimaryNav = createStackNavigator(
  {
    Cart: {screen: Cart},
    CartDetail: {screen: CartDetail},
    CartCheckout: {screen: CartCheckout},
    SnapPayment: {screen: SnapPayment},
    Reset: {screen: Reset},
    ForgotRequest: {screen: ForgotRequest},
    ForgotToken: {screen: ForgotToken},
    ResetPassword: {screen: ResetPassword},
    Register: {screen: Register},
    Login: {screen: Login},
    OnBoarding: {screen: OnBoarding},
    LaunchScreen: {screen: LaunchScreen},
    Home: {screen: HomeNav},
    Account: {screen: Account},
    Profile: {screen: Profile},
    Transaction: {screen: Transaction},
    TransactionDetail: {screen: TransactionDetail},
    MyEvent: {screen: MyEvent},
    MyEventDetail: {screen: MyEventDetail},
    EventDetail: {screen: EventDetail},
    EventPackages: {screen: EventPackages},
    EventAgenda: {screen: EventAgenda},
    EventWorkshops: {screen: EventWorkshops},
    EventContent: {screen: EventContent},
    EventRegister: {screen: EventRegister},
  },
  {
    // Default config for all screens
    headerMode: 'none',
    initialRouteName: 'Home',
    navigationOptions: {
      headerStyle: styles.header,
    },
  },
);

export default createAppContainer(PrimaryNav);
