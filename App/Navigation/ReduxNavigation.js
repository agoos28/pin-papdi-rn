import * as React from 'react';
import {BackHandler, Platform} from 'react-native';
import {
  createReactNavigationReduxMiddleware,
  createReduxContainer,
} from 'react-navigation-redux-helpers';
import {connect} from 'react-redux';
import AppNavigation from './AppNavigation';
import SafeAreaView from 'react-native-safe-area-view'

export const appNavigatorMiddleware = createReactNavigationReduxMiddleware(
  (state) => state.nav,
  'root',
);

const ReduxAppNavigator = createReduxContainer(AppNavigation, 'root');

class ReduxNavigation extends React.Component {
  componentDidMount() {
    if (Platform.OS === 'ios') {
      return;
    }
    BackHandler.addEventListener('hardwareBackPress', () => {
      const {dispatch, nav} = this.props;

      // change to whatever is your first screen, otherwise unpredictable results may occur
      const currentRoute = nav.routes[nav.index];
      if (currentRoute.routeName === 'Home') {
        if (!currentRoute.index) {
          BackHandler.exitApp();
          return false;
        }
      }
      // if (shouldCloseApp(nav)) return false
      dispatch({type: 'Navigation/BACK'});
      return true;
    });
  }

  componentWillUnmount() {
    if (Platform.OS === 'ios') {
      return;
    }
    BackHandler.removeEventListener('hardwareBackPress', undefined);
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <ReduxAppNavigator
          dispatch={this.props.dispatch}
          state={this.props.nav}
        />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  nav: state.nav,
});

export default connect(mapStateToProps)(ReduxNavigation);
