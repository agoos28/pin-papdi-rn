import {createReducer, createActions} from 'reduxsauce';
import Immutable from 'seamless-immutable';
import {Alert} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import fa from 'moment/locale/fa'

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  authRequest: ['task', 'params'],
  authSuccess: ['method', 'data'],
  authFailure: ['error'],
});

export const AuthTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  guest: true,
  user: null,
  password: null,
  tokens: null,
  fetching: false,
  error: null,
});

/* ------------- Selectors ------------- */

export const AuthSelectors = {
  getUser: (state) => state.user,
  isGuest: (state) => state.guest,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const authRequest = (state, action) => {
  const {task, params} = action;
  switch (task) {
    case 'login':
      return state.merge({
        password: params.passwd,
        fetching: true,
      });
    default:
      return state.merge({fetching: true, error: null});
  }
};

// successful api lookup
export const authSuccess = (state, action) => {
  const {method, data} = action;
  switch (method) {
    case 'login':
      if (!data.id || data.id === 0) {
        return state.merge(
          {user: data, guest: true, fetching: false, error: null},
          {deep: true},
        );
      }
      AsyncStorage.setItem('isLogin', true);
      return state.merge(
        {user: data, guest: false, fetching: false, error: null},
        {deep: true},
      );
    case 'register_save':
      if (!!data.id && data.id !== 0) {
        AsyncStorage.setItem('isLogin', true);
        return state.merge(
          {user: data, guest: false, fetching: false, error: null},
          {deep: true},
        );
      }
      return state;
    case 'getProfile':
      if (!!data.id && data.id !== 0) {
        AsyncStorage.setItem('isLogin', true);
        return state.merge(
          {user: data, guest: false, fetching: false, error: null},
          {deep: true},
        );
      }
      return state;
    case 'save':
      if (!!data.id && data.id !== 0) {
        AsyncStorage.setItem('isLogin', true);
        return state.merge(
          {user: data, guest: false, fetching: false, error: null},
          {deep: true},
        );
      }
      return state;
    case 'updatePropict':
      if (!!data.id && data.id !== 0) {
        return state.merge(
          {user: data, guest: false, fetching: false, error: null},
          {deep: true},
        );
      }
      return state;
    case 'refreshToken':
      return state.merge({tokens: data});
    case 'logout':
      AsyncStorage.setItem('isLogin', false);
      return INITIAL_STATE;
    default:
      return state;
  }
};

// Something went wrong somewhere.
export const authFailure = (state, payload) => {
  let errorMessage;
  if (payload.error.message) {
    errorMessage = payload.error.message;
  }
  if (payload.error.display_message) {
    errorMessage = payload.error.display_message;
  }
  Alert.alert('Error', errorMessage || 'Opps!! Something went wrong');
  return state.merge({fetching: false, error: true});
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.AUTH_REQUEST]: authRequest,
  [Types.AUTH_SUCCESS]: authSuccess,
  [Types.AUTH_FAILURE]: authFailure,
});
