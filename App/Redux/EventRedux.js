import {createReducer, createActions} from 'reduxsauce';
import Immutable from 'seamless-immutable';
import {Alert} from 'react-native';

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  eventRequest: ['task', 'params'],
  eventSuccess: ['method', 'data'],
  eventFailure: ['error'],
});

export const EventTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  cart: null,
  fetching: null,
  payload: null,
  error: null,
});

/* ------------- Selectors ------------- */

export const EventSelectors = {
  getData: (state) => state.data,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, action) => {
  const {task, params} = action;
  switch (task) {
    case 'login':
      return state.merge({
        password: params.password,
        fetching: true,
        error: null,
      });
    default:
      return state.merge({fetching: true, error: null});
  }
};

// successful api lookup
export const success = (state, action) => {
  const {method, data} = action;
  switch (method) {
    case 'cart':
      return state.merge({fetching: false, error: false, cart: data});
    default:
      return state;
  }
};

// Something went wrong somewhere.
export const failure = (state) =>
  state.merge({fetching: false, error: true, payload: null});

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.EVENT_REQUEST]: request,
  [Types.EVENT_SUCCESS]: success,
  [Types.EVENT_FAILURE]: failure,
});
