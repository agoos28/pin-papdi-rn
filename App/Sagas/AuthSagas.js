/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the Infinite Red Slack channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 *************************************************************/

import {call, put} from 'redux-saga/effects';
import AuthActions from '../Redux/AuthRedux';
// import { AuthSelectors } from '../Redux/AuthRedux'

export function* Auth(api, action) {
  let response = null;
  try {
    response = yield call(api.user, action.task, action.params);
    if (
      response !== null &&
      response.ok === true &&
      response.data !== null &&
      response.data.data !== null
    ) {
      if (action.task === 'login') {
        if (!response.data.data.id) {
          yield put(AuthActions.authFailure({message: 'Login gagal'}));
        }
      }
      yield put(AuthActions.authSuccess(action.task, response.data.data));
    } else {
      if (response.data) {
        yield put(AuthActions.authFailure(response.data));
      } else {
        yield put(AuthActions.authFailure(response.originalError));
      }
    }
  } catch (error) {
    yield put(AuthActions.authFailure(error));
  }
}
