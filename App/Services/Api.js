// a library to wrap and simplify api calls
import apisauce from 'apisauce';
import AsyncStorage from '@react-native-community/async-storage';
import AppConfig from '../Config/AppConfig';
import * as Alert from 'react-native';

const base = AppConfig.apiEndpoint;

const setCookies = async (cookies = false) => {
  if (!cookies) {
    return false;
  }
  try {
    await AsyncStorage.setItem('cookies', cookies);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

const setToken = async (token = false) => {
  if (!token) {
    return false;
  }
  try {
    await AsyncStorage.setItem('token', token);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

// our "constructor"
const create = (baseURL = base) => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Content-Type': 'multipart/form-data',
      credentials: 'include',
    },
    timeout: 20000,
  });

  api.addAsyncRequestTransform((request) => async () => {
    let cookie = await AsyncStorage.getItem('cookies');
    if (cookie) {
      // request.headers.cookie = cookie;
    }
    let token = await AsyncStorage.getItem('token');
    if (token) {
      if (request.method === 'post') {
        request.data.append(token, 1);
      }
    }
    if (request.method === 'post') {
      request.data.append('platform', 'api');
    } else {
      request.params.platform = 'api';
    }
  });

  api.addResponseTransform(async (response) => {
    const newCookie = response.headers['set-cookie']
      ? response.headers['set-cookie'][0]
      : false;
    if (newCookie) {
      await setCookies(newCookie);
    }
    if (response.data && response.data.token) {
      await setToken(response.data.token);
    }
  });

  const user = async (task, parameters) => {
    let params = new FormData();
    for (let key in parameters) {
      params.append(key, parameters[key] ? parameters[key] : '');
    }
    switch (task) {
      case 'fblogin':
        return api.post('/user?task=fblogin', params);
      case 'glogin':
        return api.post('/user?task=glogin', params);
      case 'login':
        return api.post('/login?task=login', params);
      case 'userGroups':
        return api.get('/user/user-groups');
      case 'register':
        return api.post('/login?task=register_save', params);
      case 'save':
        return api.post('/user?task=save', params);
      case 'updatePropict':
        return api.post('/user?task=propict', params);
      case 'getProfile':
        return api.get('/user');
      case 'forgotPassword':
        return api.post('/login?task=requestreset', params);
      case 'confirmReset':
        return api.post('/login?task=confirmreset', params);
      case 'completereset':
        return api.post('/login?task=completereset', params);
      case 'logout':
        return api.get('/user?task=logout&request_type=api', params);
    }
  };

  const event = (task, parameters) => {
    let params = new FormData();
    for (let key in parameters) {
      params.append(key, parameters[key] ? parameters[key] : '');
    }
    switch (task) {
      case 'getList':
        return api.get('/events', params);
      case 'getDetail':
        return api.get('/events/' + parameters.catId + '/' + parameters.id);
      case 'register':
        return api.post('/component/eventorg', params);
      case 'cart':
        return api.post('/cart', params);
      case 'commitment':
        return api.post('/cart', params);
    }
  };

  const transaction = (task, parameters) => {
    let params = new FormData();
    for (let key in parameters) {
      params.append(key, parameters[key] ? parameters[key] : '');
    }
    switch (task) {
      case 'getTransaction':
        return api.get('/transactions?start=' + parameters.start);
      case 'getTransactionDetail':
        return api.get('/transactions?id=' + parameters.id);
      case 'getBookings':
        return api.get('/my-booking?start=' + parameters.start);
      case 'getBookingDetail':
        return api.get('/my-booking?id=' + parameters.id);
    }
  };

  return {
    user,
    event,
    transaction,
  };
};

export default {
  create,
};
