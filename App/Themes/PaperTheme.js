import {DefaultTheme} from 'react-native-paper';

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#a65381',
    secondary: '#414757',
    error: '#f55753',
    success: '#39b439',
    warning: '#f3c14e',
  },
};

export default theme;
